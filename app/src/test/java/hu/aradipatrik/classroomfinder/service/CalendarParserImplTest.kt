package hu.aradipatrik.classroomfinder.service

import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.not
import org.junit.Before
import org.junit.Ignore
import org.junit.Test
import java.io.BufferedReader
import java.io.InputStreamReader
import java.util.stream.Collectors.joining

class CalendarParserImplTest {
    private lateinit var calendarParser: CalendarParserService
    private lateinit var calendarAsString: String

    @Before
    fun setup() {
        calendarAsString = BufferedReader(
                InputStreamReader(
                        javaClass.classLoader!!.getResourceAsStream("testCal.ics")))
                .lines()
                .parallel()
                .collect(joining("\n"))

        calendarParser = CalendarParserServiceImpl();
    }

    @Test
    fun printExampleResult() {
        calendarParser.parse(calendarAsString).forEach {
            println(it)
        }
    }

    @Test
    fun testParse() {
        assertThat(calendarParser.parse(calendarAsString).size, not(0))
    }
}