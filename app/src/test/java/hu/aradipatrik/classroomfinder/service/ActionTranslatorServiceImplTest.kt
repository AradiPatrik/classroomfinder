package hu.aradipatrik.classroomfinder.service

import hu.aradipatrik.classroomfinder.R
import hu.aradipatrik.classroomfinder.db.entities.Classroom
import io.mockk.every
import io.mockk.mockk
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.core.IsEqual
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class ActionTranslatorServiceImplTest {

    private val testHeading = "testHeading"
    private val testSubText = "testSubText"
    private val testType = "corridor"
    private val notImportantString = ""
    private val notImportantInt = 0
    private val testPosition = 5
    private val testDirection = "right"
    private val destinationReached = "destinationReached"
    private val testPositionText = "testPositionText"
    private val testDirectionText = "testDirectionText"

    @Test
    fun translateTurnAction() {
        val mockResourceService = mockk<ResourceService> {
            every { getStringByName(TYPE_PREFIX + testType) } returns testHeading
            every { getStringByName(ACTION_PREFIX + testDirection) } returns testSubText
        }
        val actionTranslatorService = ActionTranslatorServiceImpl(mockResourceService)
        val cardStackItemText = actionTranslatorService.translateTurnAction(testDirection, testType)
        assertThat(cardStackItemText.heading, IsEqual(testHeading))
        assertThat(cardStackItemText.subText, IsEqual(testSubText))
    }

    @Test
    fun translateDestinationReached() {
        val testClassroom = Classroom(
                notImportantInt, notImportantString, notImportantString,
                notImportantInt, notImportantInt,
                testPosition, testDirection)
        val mockResourceService = mockk<ResourceService> {
            every { getStringById(R.string.destination_reached) } returns destinationReached
            every { getStringByName(POSITION_PREFIX + testPosition) } returns testPositionText
            every { getStringByName(DIRECTION_PREFIX + testDirection) } returns testDirectionText
            every {
                getTemplateStringById(
                        R.string.position_information, testPositionText, testDirectionText)
            } returns testSubText
        }
        val actionTranslatorService = ActionTranslatorServiceImpl(mockResourceService)
        val cardStackItemText = actionTranslatorService.translateDestinationReached(testClassroom)
        assertThat(cardStackItemText.heading, IsEqual(destinationReached))
        assertThat(cardStackItemText.subText, IsEqual(testSubText))
    }
}