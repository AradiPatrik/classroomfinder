package hu.aradipatrik.classroomfinder.service
import android.net.Uri
import io.mockk.every
import io.mockk.mockk
import io.mockk.mockkStatic
import io.mockk.verify
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class GoogleMapsServiceImplTest {

    private val testBuildingName = "testBuildingName"

    @Test
    fun navigateTo() {
        val mockActivityStarter = mockk<ActivityStarterService>(relaxUnitFun = true)
        val mockNotificationService = mockk<NotificationService>(relaxUnitFun = true)
        mockkStatic(Uri::class)

        every { Uri.parse(any()) } returns Uri.EMPTY

        val mapsService = GoogleMapsServiceImpl(mockActivityStarter, mockNotificationService)

        mapsService.navigateTo(testBuildingName)

        verify {
            mockNotificationService.showBackToAppNotification()
            mockActivityStarter.startActivity(any())
            Uri.parse(any())
        }
    }
}