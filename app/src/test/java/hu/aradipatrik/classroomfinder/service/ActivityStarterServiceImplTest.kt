package hu.aradipatrik.classroomfinder.service

import androidx.core.content.ContextCompat
import com.afollestad.materialdialogs.MaterialDialog
import io.mockk.Runs
import io.mockk.every
import io.mockk.just
import io.mockk.mockk
import io.mockk.mockkStatic
import io.mockk.verify
import org.junit.Test

import org.junit.Assert.*

class ActivityStarterServiceImplTest {
    @Test
    fun startActivity() {
        // Given
        mockkStatic(ContextCompat::class)
        every { ContextCompat.startActivity(any(), any(), any()) } just Runs
        val activityStarterService = ActivityStarterServiceImpl(mockk(relaxed = true))

        // When
        activityStarterService.startActivity(mockk())

        // Then
        verify { ContextCompat.startActivity(any(), any(), any()) }
    }
}