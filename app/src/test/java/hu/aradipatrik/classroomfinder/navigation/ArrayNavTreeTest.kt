package hu.aradipatrik.classroomfinder.navigation

import hu.aradipatrik.classroomfinder.db.NAV_NODE_NO_IMAGE
import hu.aradipatrik.classroomfinder.db.entities.NavNode
import org.junit.Assert.assertEquals
import org.junit.Test

class ArrayNavTreeTest {
    private val testBuildingId = 2
    private val testType = "corridor"
    private val startNodeMarkingAction = "none"
    private val startNodeParent = null
    private val actionLeft = "left"
    private val actionRight = "right"

    @Test
    fun getRouteToNode() {
        val navNode1 = NavNode(1, testBuildingId, startNodeParent,
                startNodeMarkingAction, testType, NAV_NODE_NO_IMAGE)
        val navNode2 = NavNode(2, testBuildingId, navNode1.id,
                actionLeft, testType, NAV_NODE_NO_IMAGE)
        val navNode3 = NavNode(3, testBuildingId, navNode2.id,
                actionLeft, testType, NAV_NODE_NO_IMAGE)
        val navNode4 = NavNode(4, testBuildingId, navNode3.id,
                actionLeft, testType, NAV_NODE_NO_IMAGE)
        val navNode5 = NavNode(5, testBuildingId, navNode3.id,
                actionRight, testType, NAV_NODE_NO_IMAGE)
        val navNode6 = NavNode(6, testBuildingId, navNode5.id,
                actionRight, testType, NAV_NODE_NO_IMAGE)

        // nodes are given in random order
        val navTree = NavTreeFactory().createFromNavNodeList(listOf(
                navNode4,
                navNode1,
                navNode3,
                navNode5,
                navNode2,
                navNode6
        ))

        // this means the algorithm walks right first on the tree
        assertEquals(navTree.getDepthFirstOrdering(), listOf(
                navNode1, navNode2, navNode3, navNode5, navNode6, navNode4
        ))

        assertEquals(navTree.getRouteToNode(navNode6.id), listOf(
                navNode1, navNode2, navNode3, navNode5, navNode6
        ))
    }
}