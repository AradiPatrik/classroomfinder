package hu.aradipatrik.classroomfinder.testing

@Target(AnnotationTarget.CLASS)
annotation class OpenForTesting
