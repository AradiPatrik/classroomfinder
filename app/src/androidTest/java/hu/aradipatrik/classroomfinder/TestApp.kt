package hu.aradipatrik.classroomfinder

import android.app.Application

/**
 * We use a separate App for tests to prevent initializing dependency injection.
 *
 * See [hu.aradipatrik.classroomfinder.util.ClassroomFinderTestRunner].
 */
class TestApp : Application()
