package hu.aradipatrik.classroomfinder.service

import androidx.test.InstrumentationRegistry
import kotlinx.coroutines.runBlocking
import org.hamcrest.CoreMatchers.not
import org.junit.Test

import org.junit.Assert.*
import android.R.string
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.Matchers.isEmptyString


class VolleyHttpServiceTest {

    @Test
    fun getString() {
        val calendarString = runBlocking { VolleyHttpService(InstrumentationRegistry.getTargetContext()).getString("https://postman-echo.com/get") }
        assertThat(calendarString, not(isEmptyString()))
    }
}