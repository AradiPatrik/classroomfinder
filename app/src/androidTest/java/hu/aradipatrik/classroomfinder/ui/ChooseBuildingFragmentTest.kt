package hu.aradipatrik.classroomfinder.ui

import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavController
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.hasDescendant
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.rule.ActivityTestRule
import androidx.test.runner.AndroidJUnit4
import hu.aradipatrik.classroomfinder.FabManager
import hu.aradipatrik.classroomfinder.R
import hu.aradipatrik.classroomfinder.db.entities.Building
import hu.aradipatrik.classroomfinder.service.GoogleMapsService
import hu.aradipatrik.classroomfinder.service.ImageLoaderService
import hu.aradipatrik.classroomfinder.testing.SingleFragmentActivity
import hu.aradipatrik.classroomfinder.ui.buildings.ChooseBuildingFragment
import hu.aradipatrik.classroomfinder.ui.buildings.ChooseBuildingFragmentDirections
import hu.aradipatrik.classroomfinder.ui.buildings.ChooseBuildingViewModel
import hu.aradipatrik.classroomfinder.util.CountingAppExecutorsRule
import hu.aradipatrik.classroomfinder.util.DataBindingIdlingResourceRule
import hu.aradipatrik.classroomfinder.util.RecyclerViewMatcher
import hu.aradipatrik.classroomfinder.util.TaskExecutorWithIdlingResourceRule
import hu.aradipatrik.classroomfinder.util.ViewModelUtil
import hu.aradipatrik.classroomfinder.util.createBuilding
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import io.mockk.verifyAll
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class ChooseBuildingFragmentTest {
    @Rule
    @JvmField
    val activityRule = ActivityTestRule(
            SingleFragmentActivity::class.java, true, true)
    @Rule
    @JvmField
    val executorRule = TaskExecutorWithIdlingResourceRule()
    @Rule
    @JvmField
    val countingAppExecutors = CountingAppExecutorsRule()
    @Rule
    @JvmField
    val dataBindingIdlingResourceRule = DataBindingIdlingResourceRule(activityRule)

    private val chooseBuildingFragment = TestChooseBuildingFragment()
    private val fabManager = mockk<FabManager>(relaxUnitFun = true)
    private val googleMapsService = mockk<GoogleMapsService>(relaxUnitFun = true)
    private val imageLoaderService = mockk<ImageLoaderService>(relaxUnitFun = true)
    private val viewModel = mockk<ChooseBuildingViewModel>(relaxUnitFun = true)

    private val buildingsLiveData = MutableLiveData<List<Building>>()

    @Before
    fun init() {
        chooseBuildingFragment.viewModelFactory = ViewModelUtil.createFor(viewModel)
        chooseBuildingFragment.fabManager = fabManager
        chooseBuildingFragment.googleMapsService = googleMapsService
        chooseBuildingFragment.imageLoaderService = imageLoaderService
        chooseBuildingFragment.appExecutors = countingAppExecutors.appExecutors

        every { viewModel.buildings } returns buildingsLiveData

        activityRule.activity.setFragment(chooseBuildingFragment)
    }

    @Test
    fun loadBuildings() {
        val testBuildings = listOf(
                createBuilding(name = "testBuildingName1", streetName = "testStreetName1"),
                createBuilding(name = "testBuildingName2", streetName = "testStreetName2"))
        buildingsLiveData.postValue(testBuildings)

        testBuildings[0] wasLoadedAtPosition 0
        testBuildings[1] wasLoadedAtPosition 1
    }

    private infix fun Building.wasLoadedAtPosition(position: Int) {
        onView(listMatcher().at(position)).check(matches(hasDescendant(withText(name))))
        onView(listMatcher().at(position)).check(matches(hasDescendant(withText(streetName))))
        verify { imageLoaderService.loadImageInto(imageUrl!!, any()) }
    }

    @Test
    fun classroomsButtonClick() {
        val testBuilding = createBuilding()
        buildingsLiveData.postValue(listOf(testBuilding))

        onView(withId(R.id.classroomsButton)).perform(click())

        verifyAll {
            viewModel.buildings
            chooseBuildingFragment.navController()
                    .navigate(ChooseBuildingFragmentDirections
                            .chooseBuilding(testBuilding.id, testBuilding.name))
        }
    }

    @Test
    fun navigateButtonClick() {
        val testBuilding = createBuilding()
        buildingsLiveData.postValue(listOf(testBuilding))

        onView(withId(R.id.navigateButton)).perform(click())

        verifyAll {
            viewModel.buildings
            googleMapsService.navigateTo(testBuilding.name)
        }
    }

    private fun listMatcher() = RecyclerViewMatcher(R.id.choose_building_recycler_view)

    class TestChooseBuildingFragment : ChooseBuildingFragment() {
        val mockNavController = mockk<NavController>(relaxUnitFun = true)
        override fun navController() = mockNavController
    }
}