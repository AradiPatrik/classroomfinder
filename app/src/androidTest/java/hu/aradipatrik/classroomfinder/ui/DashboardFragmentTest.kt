package hu.aradipatrik.classroomfinder.ui

import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavController
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.rule.ActivityTestRule
import androidx.test.runner.AndroidJUnit4
import hu.aradipatrik.classroomfinder.FabManager
import hu.aradipatrik.classroomfinder.R
import hu.aradipatrik.classroomfinder.db.pokos.CourseCardItem
import hu.aradipatrik.classroomfinder.db.pokos.CourseLocation
import hu.aradipatrik.classroomfinder.service.GoogleMapsService
import hu.aradipatrik.classroomfinder.service.ImageLoaderService
import hu.aradipatrik.classroomfinder.testing.SingleFragmentActivity
import hu.aradipatrik.classroomfinder.ui.dashboard.DashboardFragment
import hu.aradipatrik.classroomfinder.ui.dashboard.DashboardFragmentDirections
import hu.aradipatrik.classroomfinder.ui.dashboard.DashboardViewModel
import hu.aradipatrik.classroomfinder.util.CountingAppExecutorsRule
import hu.aradipatrik.classroomfinder.util.DataBindingIdlingResourceRule
import hu.aradipatrik.classroomfinder.util.TaskExecutorWithIdlingResourceRule
import hu.aradipatrik.classroomfinder.util.ViewModelUtil
import io.mockk.Called
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import io.mockk.verifyAll
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class DashboardFragmentTest {
    @Rule
    @JvmField
    val activityRule = ActivityTestRule(SingleFragmentActivity::class.java, true, true)
    @Rule
    @JvmField
    val executorRule = TaskExecutorWithIdlingResourceRule()
    @Rule
    @JvmField
    val countingAppExecutors = CountingAppExecutorsRule()
    @Rule
    @JvmField
    val dataBindingIdlingResourceRule = DataBindingIdlingResourceRule(activityRule)

    private val dashboardFragment: DashboardFragment = TestDashboardFragment()
    private val allCoursesLiveData = MutableLiveData<List<CourseCardItem>>()
    private val courseLocationLiveData = MutableLiveData<CourseLocation>()
    private val lecturerOfficeLiveData = MutableLiveData<CourseLocation>()

    private val googleMapsService = mockk<GoogleMapsService>(relaxUnitFun = true)
    private val fabManager = mockk<FabManager>(relaxUnitFun = true)
    private val imageLoaderService = mockk<ImageLoaderService>(relaxUnitFun = true)
    private val viewModel = mockk<DashboardViewModel>(relaxUnitFun = true)

    private val testCourseCode = "testCourseCode"
    private val testCourseName = "testCourseName"
    private val testLecturer = "testLecturer"
    private val testUrl = "testUrl"
    private val testCourseId = 2
    private val testLecturerId = 3
    private val testCourseLocationName = "testCourseLocationName"
    private val testCourseLocationId = 4
    private val testLecturerOfficeName = "testLecturerOfficeName"
    private val testLecturerOfficeId = 5

    @Before
    fun init() {
        every { viewModel.allCourses } returns allCoursesLiveData
        every { viewModel.getCourseLocation(any()) } returns courseLocationLiveData
        every { viewModel.getLecturerOfficeOfCourse(any()) } returns lecturerOfficeLiveData

        dashboardFragment.appExecutors = countingAppExecutors.appExecutors
        dashboardFragment.viewModelFactory = ViewModelUtil.createFor(viewModel)
        dashboardFragment.googleMapsService = googleMapsService
        dashboardFragment.fabManager = fabManager
        dashboardFragment.imageLoaderService = imageLoaderService
        activityRule.activity.setFragment(dashboardFragment)
    }

    @Test
    fun fragmentShouldShowFabManagerWithPlusIconAndSetItsCallback() {
        verifyAll {
            fabManager.setFabVisible(true)
            fabManager.setFabIcon(R.drawable.ic_add_white)
            fabManager.setFabClickListener(any())
        }
    }

    private fun loadTestCourse() {
        allCoursesLiveData.postValue(listOf(CourseCardItem(
                courseId = testCourseId,
                lecturerId = testLecturerId,
                courseCode = testCourseCode, // TODO: remove this attribute because it is unused
                courseName = testCourseName,
                lecturerName = testLecturer,
                buildingImage = testUrl
        )))
    }

    @Test
    fun loadCourses() {
        // Given
        loadTestCourse()

        // When
        onView(withId(R.id.lecturer_name)).check(matches(withText(testLecturer)))
        onView(withId(R.id.lecture_name)).check(matches(withText(testCourseName)))

        // Then
        verify { imageLoaderService.loadImageInto(imageUrl = testUrl, imageView = any()) }
    }

    @Test
    fun navigateToClassroom() {
        // Given
        loadTestCourse()
        courseLocationLiveData.apply { postValue(CourseLocation(testCourseLocationName, testCourseLocationId)) }
        every { viewModel.getCourseLocation(any()) } returns courseLocationLiveData

        // When
        onView(withId(R.id.buttonToCourse)).perform(click())

        // Then
        verifyAll {
            googleMapsService.navigateTo(testCourseLocationName)
            dashboardFragment.navController().navigate(
                    DashboardFragmentDirections.navigateToClassroom(testCourseLocationId))
        }
    }

    @Test
    fun navigateToLecturer() {
        // Given
        loadTestCourse()
        lecturerOfficeLiveData.apply {
            postValue(CourseLocation(testLecturerOfficeName, testLecturerOfficeId))
        }
        every { viewModel.getLecturerOfficeOfCourse(any()) } returns lecturerOfficeLiveData

        // When
        onView(withId(R.id.buttonToLecturer)).perform(click())

        // Then
        verifyAll {
            googleMapsService.navigateTo(testLecturerOfficeName)
            dashboardFragment.navController().navigate(
                    DashboardFragmentDirections.navigateToClassroom(testLecturerOfficeId))
        }
    }

    @Test
    fun shouldNotBeAbleToNavigateToCourseWhichClassroomIsNull() {
        // Given
        loadTestCourse()
        courseLocationLiveData.apply { postValue(null) }
        every { viewModel.getCourseLocation(any()) } returns courseLocationLiveData

        // When
        onView(withId(R.id.buttonToCourse)).perform(click())

        // Then
        verify { listOf(googleMapsService, dashboardFragment.navController()) wasNot Called }
    }

    @Test
    fun shouldNotBeAbleToLecturerWhoseOfficeIsNull() {
        // Given
        loadTestCourse()
        lecturerOfficeLiveData.apply { postValue(null) }
        every { viewModel.getLecturerOfficeOfCourse(any()) } returns lecturerOfficeLiveData

        // When
        onView(withId(R.id.buttonToLecturer))

        // Then
        verify { listOf(googleMapsService, dashboardFragment.navController()) wasNot Called }
    }

    class TestDashboardFragment : DashboardFragment() {
        val mockNavController = mockk<NavController>(relaxUnitFun = true)
        override fun navController() = mockNavController
    }
}