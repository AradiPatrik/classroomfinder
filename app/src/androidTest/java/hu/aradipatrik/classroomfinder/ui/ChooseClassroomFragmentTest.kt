package hu.aradipatrik.classroomfinder.ui

import androidx.core.os.bundleOf
import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavController
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.hasDescendant
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.rule.ActivityTestRule
import androidx.test.runner.AndroidJUnit4
import hu.aradipatrik.classroomfinder.FabManager
import hu.aradipatrik.classroomfinder.R
import hu.aradipatrik.classroomfinder.SearchViewOwner
import hu.aradipatrik.classroomfinder.db.pokos.ClassroomEssentials
import hu.aradipatrik.classroomfinder.service.GoogleMapsService
import hu.aradipatrik.classroomfinder.testing.SingleFragmentActivity
import hu.aradipatrik.classroomfinder.ui.classrooms.ChooseClassroomFragment
import hu.aradipatrik.classroomfinder.ui.classrooms.ChooseClassroomFragmentDirections
import hu.aradipatrik.classroomfinder.ui.classrooms.ChooseClassroomViewModel
import hu.aradipatrik.classroomfinder.util.CountingAppExecutorsRule
import hu.aradipatrik.classroomfinder.util.DataBindingIdlingResourceRule
import hu.aradipatrik.classroomfinder.util.RecyclerViewMatcher
import hu.aradipatrik.classroomfinder.util.STRING_DOES_NOT_MATTER
import hu.aradipatrik.classroomfinder.util.TaskExecutorWithIdlingResourceRule
import hu.aradipatrik.classroomfinder.util.ViewModelUtil
import hu.aradipatrik.classroomfinder.util.createBuilding
import hu.aradipatrik.classroomfinder.util.createClassroomEssentials
import hu.aradipatrik.classroomfinder.view.custom.SearchViewWrapper
import io.mockk.every
import io.mockk.mockk
import io.mockk.verifyAll
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class ChooseClassroomFragmentTest {
    @Rule
    @JvmField
    val activityRule = ActivityTestRule(SingleFragmentActivity::class.java, true, true)
    @Rule
    @JvmField
    val executorRule = TaskExecutorWithIdlingResourceRule()
    @Rule
    @JvmField
    val countingAppExecutors = CountingAppExecutorsRule()
    @Rule
    @JvmField
    val dataBindingIdlingResourceRule = DataBindingIdlingResourceRule(activityRule)

    private val chooseClassroomFragment = TestChooseClassroomFragment()

    private val fabManager = mockk<FabManager>(relaxUnitFun = true)
    private val googleMapsService = mockk<GoogleMapsService>(relaxUnitFun = true)
    private val searchViewOwner = mockk<SearchViewOwner>(relaxUnitFun = true)
    private val searchView = mockk<SearchViewWrapper>(relaxUnitFun = true)
    private val viewModel = mockk<ChooseClassroomViewModel>(relaxUnitFun = true)

    private val selectedBuilding = createBuilding()
    private val classroomsLiveData = MutableLiveData<List<ClassroomEssentials>>()

    @Before
    fun init() {
        every { searchViewOwner.getSearchView() } returns searchView
        every { viewModel.classrooms } returns classroomsLiveData
        every { viewModel.getBuildingName() } returns STRING_DOES_NOT_MATTER

        chooseClassroomFragment.fabManager = fabManager
        chooseClassroomFragment.googleMapsService = googleMapsService
        chooseClassroomFragment.searchViewOwner = searchViewOwner
        chooseClassroomFragment.appExecutors = countingAppExecutors.appExecutors
        chooseClassroomFragment.viewModelFactory = ViewModelUtil.createFor(viewModel)

        chooseClassroomFragment.arguments =
                bundleOf(Pair("buildingId", 0), Pair("buildingName", STRING_DOES_NOT_MATTER))

        activityRule.activity.setFragment(chooseClassroomFragment)
    }

    @Test
    fun loadClassrooms() {
        val testClassroom0 = createClassroomEssentials(name = "testName0", code = "testCode0")
        val testClassroom1 = createClassroomEssentials(name = "testName1", code = "testCode1")
        classroomsLiveData.postValue(listOf(testClassroom0, testClassroom1))

        verifyAll {
            viewModel.classrooms
            viewModel.setBuilding(any(), any())
            testClassroom0 wasLoadedAtPosition 0
            testClassroom1 wasLoadedAtPosition 1
        }
    }

    @Test
    fun onClassroomItemClick() {
        val testClassroom = createClassroomEssentials()
        classroomsLiveData.postValue(listOf(testClassroom))

        onView(listMatcher().at(0)).perform(click())

        verifyAll {
            searchView.onShow(any())
            searchView.onQueryChange(any())
            searchView.closeSearch()
            testClassroom wasLoadedAtPosition 0
            googleMapsService.navigateTo(selectedBuilding.name)
            chooseClassroomFragment.mockNavController.navigate(
                    ChooseClassroomFragmentDirections.navigateToClassroom(testClassroom.id))
        }
    }

    private infix fun ClassroomEssentials.wasLoadedAtPosition(position: Int) {
        onView(listMatcher().at(position)).check(matches(hasDescendant(withText(name))))
        onView(listMatcher().at(position)).check(matches(hasDescendant(withText(code))))
    }

    private fun listMatcher() = RecyclerViewMatcher(R.id.choose_classroom_recycler_view)

    class TestChooseClassroomFragment : ChooseClassroomFragment() {
        val mockNavController = mockk<NavController>(relaxUnitFun = true)
        override fun navController() = mockNavController
    }
}