package hu.aradipatrik.classroomfinder.db

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import hu.aradipatrik.classroomfinder.db.entities.Building
import hu.aradipatrik.classroomfinder.db.entities.Classroom
import hu.aradipatrik.classroomfinder.db.entities.Course
import hu.aradipatrik.classroomfinder.db.entities.NavNode
import hu.aradipatrik.classroomfinder.util.LiveDataTestUtil.getValue
import hu.aradipatrik.classroomfinder.util.createCourse
import hu.aradipatrik.classroomfinder.util.testBuilding
import hu.aradipatrik.classroomfinder.util.testClassroom
import hu.aradipatrik.classroomfinder.util.testLecturer
import hu.aradipatrik.classroomfinder.util.testNavNode
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.notNullValue
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Rule
import org.junit.Test

class CourseDaoTest : DbTest() {
    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    private fun insertCourseWithForeignKeyConstraints(
        course: Course,
        classroom: Classroom = testClassroom,
        node: NavNode = testNavNode,
        building: Building = testBuilding
    ) {
        db.buildingDao().insertAll(listOf(building))
        db.navNodeDao().insertAll(listOf(node))
        db.classroomDao().insertAll(listOf(classroom))
        db.lecturerDao().insertAll(listOf(testLecturer))
        db.courseDao().insertAll(listOf(course))
    }

    @Test
    fun insertAndRead() {
        // Given
        val course = createCourse(lecturerId = testLecturer.id, classroomId = testClassroom.id)

        // When
        insertCourseWithForeignKeyConstraints(course = course)
        val loaded = getValue(db.courseDao().getAllCourses())

        // Then
        assertThat(loaded, notNullValue())
        assertThat(loaded.size, `is`(1))
        assertThat(loaded[0].lecturerId, `is`(course.lecturerId))
        assertThat(loaded[0].courseId, `is`(course.id))
        assertThat(loaded[0].courseCode, `is`(course.code))
        assertThat(loaded[0].courseName, `is`(course.name))
        assertThat(loaded[0].lecturerName, `is`(testLecturer.name))
        assertThat(loaded[0].buildingImage, `is`(testBuilding.imageUrl))
    }

    @Test
    fun getCourseLocation() {
        // Given
        val course = createCourse(lecturerId = testLecturer.id, classroomId = testClassroom.id)

        // When
        insertCourseWithForeignKeyConstraints(course = course)
        val location = getValue(db.courseDao().getCourseLocation(course.id))

        // Then
        assertThat(location, notNullValue())
        assertThat(location.buildingName, `is`(testBuilding.name))
        assertThat(location.classroomId, `is`(testClassroom.id))
    }

    @Test
    fun deleteAll() {
        // Given
        val course = createCourse(lecturerId = testLecturer.id, classroomId = testClassroom.id)

        // When
        insertCourseWithForeignKeyConstraints(course = course)
        db.courseDao().deleteAll()
        val remainingCourses = getValue(db.courseDao().getAllCourses())

        // Then
        assertThat(remainingCourses, notNullValue())
        assertThat(remainingCourses.size, `is`(0))
    }
}