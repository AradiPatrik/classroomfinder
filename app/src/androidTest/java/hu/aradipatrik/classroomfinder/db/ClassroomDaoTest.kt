package hu.aradipatrik.classroomfinder.db

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import hu.aradipatrik.classroomfinder.db.entities.Building
import hu.aradipatrik.classroomfinder.db.entities.Classroom
import hu.aradipatrik.classroomfinder.db.entities.NavNode
import hu.aradipatrik.classroomfinder.util.LiveDataTestUtil.getValue
import hu.aradipatrik.classroomfinder.util.createBuilding
import hu.aradipatrik.classroomfinder.util.createClassroom
import hu.aradipatrik.classroomfinder.util.createNavNode
import hu.aradipatrik.classroomfinder.util.testBuilding
import hu.aradipatrik.classroomfinder.util.testNavNode
import junit.framework.TestCase.assertEquals
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.notNullValue
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Rule
import org.junit.Test

class ClassroomDaoTest : DbTest() {
    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    private val anotherBuilding = createBuilding()
    private val anotherNode = createNavNode(buildingId = anotherBuilding.id, parentId = null)

    private fun insertClassroomsUnderNodeAndBuilding(
        classrooms: List<Classroom>,
        node: NavNode = testNavNode,
        building: Building = testBuilding
    ) {
        db.buildingDao().insertAll(listOf(building))
        db.navNodeDao().insertAll(listOf(node))
        db.classroomDao().insertAll(classrooms)
    }

    @Test
    fun insertAndRead() {
        // Given
        val classroom = createClassroom(
                buildingId = testBuilding.id, navNodeId = testNavNode.id)

        // When
        insertClassroomsUnderNodeAndBuilding(classrooms = listOf(classroom))
        val loaded = getValue(db.classroomDao().getAllClassrooms())

        // Then
        assertThat(loaded, notNullValue())
        assertThat(loaded.size, `is`(1))
        assertThat(loaded[0].code, `is`(classroom.code))
        assertThat(loaded[0].id, `is`(classroom.id))
        assertThat(loaded[0].name, `is`(classroom.name))
    }

    private fun fillTwoBuildingsWithClassrooms() {
        insertClassroomsUnderNodeAndBuilding(
                classrooms = getClassroomsUnderTestBuildingAndTestNode(),
                building = testBuilding,
                node = testNavNode
        )
        insertClassroomsUnderNodeAndBuilding(
                classrooms = getClassroomsUnderAnotherBuildingAndAnotherNode(),
                building = anotherBuilding,
                node = anotherNode
        )
    }

    private fun getClassroomsUnderAnotherBuildingAndAnotherNode(): List<Classroom> {
        return listOf(
                createClassroom(name = "aBcDeF",
                        buildingId = anotherBuilding.id, navNodeId = anotherNode.id)
        )
    }

    private fun getClassroomsUnderTestBuildingAndTestNode(): List<Classroom> {
        return listOf(
                createClassroom(name = "aBcDeF",
                        buildingId = testBuilding.id, navNodeId = testNavNode.id),
                createClassroom(name = "Def",
                        buildingId = testBuilding.id, navNodeId = testNavNode.id),
                createClassroom(code = "efG",
                        buildingId = testBuilding.id, navNodeId = testNavNode.id),
                createClassroom(code = "XYZ",
                        buildingId = testBuilding.id, navNodeId = testNavNode.id),
                createClassroom(code = "cDe",
                        buildingId = testBuilding.id, navNodeId = testNavNode.id)
        )
    }

    @Test
    fun getClassroomsOfBuildingWithNameOrCodeLike() {
        // Given
        val subStrThatMatches3ClassroomsInsideTestBuilding = "ef"

        // When
        fillTwoBuildingsWithClassrooms()
        val filtered = getValue(db.classroomDao().getClassroomsOfBuildingWithNameOrCodeLike(
                testBuilding.id,
                subStrThatMatches3ClassroomsInsideTestBuilding
        ))

        // Then
        assertThat(filtered, notNullValue())
        assertThat(filtered.size, `is`(3))
    }

    @Test
    fun getClassroomsWithNameOrCodeContaining() {
        // Given
        val subStrThatMatches4ClassroomsInsideEntireDb = "ef"

        // When
        fillTwoBuildingsWithClassrooms()
        val filtered = getValue(db.classroomDao().getClassroomsWithNameOrCodeContaining(
                subStrThatMatches4ClassroomsInsideEntireDb
        ))
        val filteredSync = db.classroomDao().getClassroomsWithNameOrCodeContainingSync(
                subStrThatMatches4ClassroomsInsideEntireDb
        )

        // Then
        assertThat(filtered, notNullValue())
        assertThat(filtered.size, `is`(4))
        assertEquals(filtered, filteredSync)
    }

    @Test
    fun getClassroomById() {
        // Given
        val testId = 42
        val classroom = createClassroom(id = testId,
                navNodeId = testNavNode.id, buildingId = testBuilding.id)

        // When
        insertClassroomsUnderNodeAndBuilding(listOf(classroom))
        val loaded = getValue(db.classroomDao().getClassroomById(testId))

        // Then
        assertThat(loaded, notNullValue())
        assertEquals(loaded, classroom)
    }

    @Test
    fun countClassroomsOfBuildingWithCodeLike() {
        // Given
        val testId = 42
        val classroom = createClassroom(id = testId,
                navNodeId = testNavNode.id, buildingId = testBuilding.id)

        // When
        insertClassroomsUnderNodeAndBuilding(listOf(classroom))
        val count = getValue(
                db.classroomDao().isClassroomInBuilding(testBuilding.id, testId))

        // Then
        assertThat(count, `is`(1))
    }

    @Test
    fun deleteAll() {
        // Given
        val classrooms = listOf(
                createClassroom(buildingId = testBuilding.id, navNodeId = testNavNode.id),
                createClassroom(buildingId = testBuilding.id, navNodeId = testNavNode.id))

        // When
        insertClassroomsUnderNodeAndBuilding(classrooms, testNavNode, testBuilding)
        db.classroomDao().deleteAll()
        val remainingClassrooms = getValue(db.classroomDao().getAllClassrooms())

        // Then
        assertThat(remainingClassrooms, notNullValue())
        assertThat(remainingClassrooms.size, `is`(0))
    }
}