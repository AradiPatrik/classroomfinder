package hu.aradipatrik.classroomfinder.db

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import hu.aradipatrik.classroomfinder.db.entities.Building
import hu.aradipatrik.classroomfinder.db.entities.Classroom
import hu.aradipatrik.classroomfinder.db.entities.Lecturer
import hu.aradipatrik.classroomfinder.db.entities.NavNode
import hu.aradipatrik.classroomfinder.util.LiveDataTestUtil.getValue
import hu.aradipatrik.classroomfinder.util.createLecturer
import hu.aradipatrik.classroomfinder.util.testBuilding
import hu.aradipatrik.classroomfinder.util.testClassroom
import hu.aradipatrik.classroomfinder.util.testNavNode
import junit.framework.TestCase.assertEquals
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.notNullValue
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Rule
import org.junit.Test

class LecturerDaoTest : DbTest() {
    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    private fun insertLecturersWithForeignKeyConstraints(
        lecturers: List<Lecturer>,
        classroom: Classroom = testClassroom,
        node: NavNode = testNavNode,
        building: Building = testBuilding
    ) {
        db.buildingDao().insertAll(listOf(building))
        db.navNodeDao().insertAll(listOf(node))
        db.classroomDao().insertAll(listOf(classroom))
        db.lecturerDao().insertAll(lecturers)
    }

    @Test
    fun insertAndRead() {
        // Given
        val lecturer = createLecturer(officeId = testClassroom.id)

        // When
        insertLecturersWithForeignKeyConstraints(listOf(lecturer))
        val loaded = getValue(db.lecturerDao().getAllLecturers())

        // Then
        assertThat(loaded, notNullValue())
        assertThat(loaded.size, `is`(1))
        assertThat(loaded[0], `is`(lecturer))
    }

    @Test
    fun getLecturersWithNameContaining() {
        // Given
        val shouldMatchLecturers = listOf(
                createLecturer(officeId = testClassroom.id, name = "abCdeFG"),
                createLecturer(officeId = testClassroom.id, name = "BcdeF"),
                createLecturer(officeId = testClassroom.id, name = "deFGHIj")
        )
        val nonMatchingLecturers = listOf(
                createLecturer(officeId = testClassroom.id, name = "xyz")
        )
        val queryStringThatMatches3Lecturers = "def"

        // When
        insertLecturersWithForeignKeyConstraints(shouldMatchLecturers + nonMatchingLecturers)
        val filtered = getValue(db.lecturerDao().getLecturersWithNameContaining(
                queryStringThatMatches3Lecturers
        ))
        val filteredSync = db.lecturerDao().getLecturersWithNameContainingSync(
                queryStringThatMatches3Lecturers
        )

        // Then
        assertThat(filtered, notNullValue())
        assertThat(filteredSync, notNullValue())
        assertEquals(filtered, filteredSync)
        assertThat(filtered.size, `is`(3))
        assertEquals(filtered, shouldMatchLecturers)
    }

    @Test
    fun getOfficeOfLecturer() {
        // Given
        val lecturer = createLecturer(officeId = testClassroom.id)

        // When
        insertLecturersWithForeignKeyConstraints(listOf(lecturer))
        val lecturerOffice = getValue(db.lecturerDao().getOfficeOfLecturer(lecturer.id))

        // Then
        assertThat(lecturerOffice, notNullValue())
        assertThat(lecturerOffice.classroomId, `is`(testClassroom.id))
        assertThat(lecturerOffice.buildingName, `is`(testBuilding.name))
    }

    @Test
    fun deleteAll() {
        // Given
        val lecturer = createLecturer(officeId = testClassroom.id)

        // When
        insertLecturersWithForeignKeyConstraints(listOf(lecturer))
        db.lecturerDao().deleteAll()
        val remainingLecturers = getValue(db.lecturerDao().getAllLecturers())

        // Then
        assertThat(remainingLecturers, notNullValue())
        assertThat(remainingLecturers.size, `is`(0))
    }
}