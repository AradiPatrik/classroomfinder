package hu.aradipatrik.classroomfinder.db

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import hu.aradipatrik.classroomfinder.util.LiveDataTestUtil.getValue
import hu.aradipatrik.classroomfinder.util.createBuilding
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.not
import org.hamcrest.CoreMatchers.notNullValue
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.contains
import org.hamcrest.Matchers.containsInAnyOrder
import org.junit.Rule
import org.junit.Test
import java.util.Collections

class BuildingDaoTest : DbTest() {
    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()
    private val testBuildingId = 8
    private val testBuildingName = "testBuildingName"
    private val testImageUrl = "testImageUrl"
    private val testStreetName = "testStreetName"

    private fun createTestBuilding() = createBuilding(
            id = testBuildingId,
            name = testBuildingName,
            imageUrl = testImageUrl,
            streetName = testStreetName
    )

    @Test
    fun insertAndRead() {
        // Given
        val building = createTestBuilding()

        // When
        db.buildingDao().insertAll(listOf(building))
        val loaded = getValue(db.buildingDao().getAllBuildings())

        // Then
        assertThat(loaded, notNullValue())
        assertThat(loaded.size, `is`(1))
        assertThat(loaded[0].id, `is`(testBuildingId))
        assertThat(loaded[0].name, `is`(testBuildingName))
        assertThat(loaded[0].imageUrl, `is`(testImageUrl))
        assertThat(loaded[0].streetName, `is`(testStreetName))
    }

    @Test
    fun nameQueryShouldReturnBuildingsWhichContainQueryAsSubstringIgnoringCase() {
        // Given
        val shouldMatch1 = createBuilding(name = "AbCdE")
        val shouldMatch2 = createBuilding(name = "CDeF")
        val shouldNotMatch = createBuilding(name = "XYZe")

        // When
        db.buildingDao().insertAll(listOf(shouldMatch1, shouldMatch2, shouldNotMatch))
        val filtered = db.buildingDao().getBuildingsFilteredByNameSync("cde")

        // Then
        assertThat(filtered, notNullValue())
        assertThat(filtered.size, `is`(2))
        assertThat(filtered, containsInAnyOrder(shouldMatch1, shouldMatch2))
        assertThat(filtered, not(contains(shouldNotMatch)))
    }

    @Test
    fun getBuildingById() {
        // Given
        val testBuilding = createTestBuilding()

        // When
        db.buildingDao().insertAll(Collections.singletonList(testBuilding))
        val loaded = getValue(db.buildingDao().getBuildingById(testBuildingId))

        // Then
        assertThat(loaded, notNullValue())
        assertThat(loaded.id, `is`(testBuildingId))
        assertThat(loaded.imageUrl, `is`(testImageUrl))
        assertThat(loaded.streetName, `is`(testStreetName))
        assertThat(loaded.name, `is`(testBuildingName))
    }

    @Test
    fun deleteAll() {
        // Given
        val testBuildings = listOf(createTestBuilding(), createBuilding(), createBuilding())

        // When
        db.buildingDao().insertAll(testBuildings)
        db.buildingDao().deleteAll()
        val remainingBuildings = getValue(db.buildingDao().getAllBuildings())

        // Then
        assertThat(remainingBuildings, notNullValue())
        assertThat(remainingBuildings.size, `is`(0))
    }
}
