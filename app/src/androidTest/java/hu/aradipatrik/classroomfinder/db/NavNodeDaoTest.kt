package hu.aradipatrik.classroomfinder.db

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import hu.aradipatrik.classroomfinder.db.entities.Building
import hu.aradipatrik.classroomfinder.db.entities.NavNode
import hu.aradipatrik.classroomfinder.util.LiveDataTestUtil.getValue
import hu.aradipatrik.classroomfinder.util.createBuilding
import hu.aradipatrik.classroomfinder.util.createNavNode
import hu.aradipatrik.classroomfinder.util.testBuilding
import junit.framework.TestCase.assertEquals
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.notNullValue
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Rule
import org.junit.Test

class NavNodeDaoTest : DbTest() {
    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()
    private val root = createNavNode(buildingId = testBuilding.id, parentId = null)
    private val navNode1 = createNavNode(buildingId = testBuilding.id, parentId = root.id)
    private val navNode2 = createNavNode(buildingId = testBuilding.id, parentId = navNode1.id)

    private fun insertNavNodesWithForeignKeyConstraints(
        nodes: List<NavNode>,
        building: Building = testBuilding
    ) {
        db.buildingDao().insertAll(listOf(building))
        db.navNodeDao().insertAll(nodes)
    }

    @Test
    fun insertAndRead() {
        // Given
        val navNodes = listOf(root, navNode1, navNode2)
        val anotherBuilding = createBuilding()
        val navNodeUnderAnotherBuilding = createNavNode(
                parentId = null, buildingId = anotherBuilding.id)

        // When
        insertNavNodesWithForeignKeyConstraints(navNodes)
        db.buildingDao().insertAll(listOf(anotherBuilding))
        db.navNodeDao().insertAll(listOf(navNodeUnderAnotherBuilding))
        val loaded = getValue(db.navNodeDao().getNavNodesInBuilding(testBuilding.id))
        val loadedSync = db.navNodeDao().getNavNodesInBuildingSync(testBuilding.id)

        // Then
        assertThat(loaded, notNullValue())
        assertThat(loadedSync, notNullValue())
        assertEquals(loaded, loadedSync)
        assertEquals(navNodes, loaded)
    }

    @Test
    fun deleteAll() {
        // Given
        val navNode = createNavNode(parentId = null, buildingId = testBuilding.id)

        // When
        insertNavNodesWithForeignKeyConstraints(listOf(navNode))
        db.navNodeDao().deleteAll()
        val navNodesRemaining = db.navNodeDao().getNavNodesInBuildingSync(testBuilding.id)

        // Then
        assertThat(navNodesRemaining, notNullValue())
        assertThat(navNodesRemaining.size, `is`(0))
    }
}