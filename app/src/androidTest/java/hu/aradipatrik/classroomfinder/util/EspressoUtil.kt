package hu.aradipatrik.classroomfinder.util

import androidx.recyclerview.widget.RecyclerView

object EspressoUtil {
    fun disableRecyclerViewAnimations(recyclerView: RecyclerView) {
        recyclerView.itemAnimator = null
    }
}