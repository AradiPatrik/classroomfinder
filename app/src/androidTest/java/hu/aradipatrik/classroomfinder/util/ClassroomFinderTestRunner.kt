package hu.aradipatrik.classroomfinder.util

import android.app.Application
import android.content.Context
import androidx.test.runner.AndroidJUnitRunner
import hu.aradipatrik.classroomfinder.TestApp

class ClassroomFinderTestRunner : AndroidJUnitRunner() {
    override fun newApplication(cl: ClassLoader, className: String, context: Context): Application {
        return super.newApplication(cl, TestApp::class.java.name, context)
    }
}