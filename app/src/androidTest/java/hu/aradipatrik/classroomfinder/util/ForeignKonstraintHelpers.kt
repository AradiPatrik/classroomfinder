package hu.aradipatrik.classroomfinder.util

val testBuilding = createBuilding()
val testNavNode = createNavNode(buildingId = testBuilding.id, parentId = null)
val testClassroom = createClassroom(
        buildingId = testBuilding.id, navNodeId = testNavNode.id)
val testLecturer = createLecturer(officeId = testClassroom.id)