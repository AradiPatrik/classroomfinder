package hu.aradipatrik.classroomfinder.util

import hu.aradipatrik.classroomfinder.db.entities.Building
import hu.aradipatrik.classroomfinder.db.entities.Classroom
import hu.aradipatrik.classroomfinder.db.entities.Course
import hu.aradipatrik.classroomfinder.db.entities.Lecturer
import hu.aradipatrik.classroomfinder.db.entities.NavNode
import hu.aradipatrik.classroomfinder.db.pokos.ClassroomEssentials

var uniqueBuildingId = 1
var uniqueClassroomId = 1
var uniqueNavNodeId = 1
var uniqueCourseId = 1
var uniqueLecturerId = 1
var uniqueClassroomEssentialsId = 1
const val STRING_DOES_NOT_MATTER = "DOES_NOT_MATTER"
const val INT_DOES_NOT_MATTER = 42
const val INVALID_ID = -1

fun createBuilding(
    id: Int = uniqueBuildingId++,
    name: String = STRING_DOES_NOT_MATTER,
    imageUrl: String = STRING_DOES_NOT_MATTER,
    streetName: String = STRING_DOES_NOT_MATTER
) = Building(
        id = id,
        name = name,
        imageUrl = imageUrl,
        streetName = streetName
)

fun createClassroom(
    id: Int = uniqueClassroomId++,
    code: String = STRING_DOES_NOT_MATTER,
    name: String = STRING_DOES_NOT_MATTER,
    buildingId: Int = INVALID_ID,
    navNodeId: Int = INVALID_ID,
    position: Int = INT_DOES_NOT_MATTER,
    direction: String = STRING_DOES_NOT_MATTER
) = Classroom(
        id = id,
        code = code,
        name = name,
        buildingId = buildingId,
        navNodeId = navNodeId,
        position = position,
        direction = direction
)

fun createNavNode(
    id: Int = uniqueNavNodeId++,
    buildingId: Int = INVALID_ID,
    parentId: Int? = INVALID_ID,
    action: String = STRING_DOES_NOT_MATTER,
    type: String = STRING_DOES_NOT_MATTER,
    imageUrl: String = STRING_DOES_NOT_MATTER
) = NavNode(
        id = id,
        buildingId = buildingId,
        parentId = parentId,
        action = action,
        type = type,
        imageUrl = imageUrl
)

fun createCourse(
    id: Int = uniqueCourseId++,
    code: String = STRING_DOES_NOT_MATTER,
    lecturerId: Int = INVALID_ID,
    name: String = STRING_DOES_NOT_MATTER,
    classroomId: Int = INVALID_ID
) = Course(
        id = id,
        code = code,
        lecturerId = lecturerId,
        name = name,
        classroomId = classroomId
)

fun createLecturer(
    id: Int = uniqueLecturerId++,
    code: String = STRING_DOES_NOT_MATTER,
    email: String = STRING_DOES_NOT_MATTER,
    name: String = STRING_DOES_NOT_MATTER,
    profilePicture: String = STRING_DOES_NOT_MATTER,
    officeId: Int = INVALID_ID
) = Lecturer(
        id = id,
        code = code,
        email = email,
        name = name,
        profilePicture = profilePicture,
        officeId = officeId
)

fun createClassroomEssentials(
    id: Int = uniqueClassroomEssentialsId++,
    code: String = STRING_DOES_NOT_MATTER,
    name: String = STRING_DOES_NOT_MATTER
) = ClassroomEssentials(
        id = id,
        code = code,
        name = name
)
