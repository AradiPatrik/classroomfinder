package hu.aradipatrik.classroomfinder.db.pokos

import androidx.room.ColumnInfo

data class ClassroomEssentials(
    @ColumnInfo(name = "id")
    val id: Int,
    @ColumnInfo(name = "code")
    val code: String,
    @ColumnInfo(name = "name")
    val name: String
)