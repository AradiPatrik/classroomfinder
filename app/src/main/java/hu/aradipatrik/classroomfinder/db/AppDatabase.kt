package hu.aradipatrik.classroomfinder.db

import androidx.room.Database
import androidx.room.RoomDatabase
import hu.aradipatrik.classroomfinder.db.daos.BuildingDao
import hu.aradipatrik.classroomfinder.db.daos.ClassroomDao
import hu.aradipatrik.classroomfinder.db.daos.CourseDao
import hu.aradipatrik.classroomfinder.db.daos.LecturerDao
import hu.aradipatrik.classroomfinder.db.daos.NavNodeDao
import hu.aradipatrik.classroomfinder.db.entities.Building
import hu.aradipatrik.classroomfinder.db.entities.Classroom
import hu.aradipatrik.classroomfinder.db.entities.Course
import hu.aradipatrik.classroomfinder.db.entities.Lecturer
import hu.aradipatrik.classroomfinder.db.entities.NavNode

@Database(entities = [Building::class, Classroom::class,
    Course::class, Lecturer::class, NavNode::class], version = 4, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {
    abstract fun buildingDao(): BuildingDao
    abstract fun classroomDao(): ClassroomDao
    abstract fun courseDao(): CourseDao
    abstract fun lecturerDao(): LecturerDao
    abstract fun navNodeDao(): NavNodeDao
}