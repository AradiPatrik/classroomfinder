package hu.aradipatrik.classroomfinder.db.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.ForeignKey.CASCADE
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(
        tableName = "lecturers",
        foreignKeys = [ForeignKey(
                entity = Classroom::class,
                parentColumns = ["id"],
                childColumns = ["office_id"],
                onDelete = CASCADE)
        ],
        indices = [Index(value = ["office_id"])]
)
data class Lecturer(
    @PrimaryKey @ColumnInfo(name = "id") val id: Int,
    @ColumnInfo(name = "code") val code: String,
    @ColumnInfo(name = "email") val email: String,
    @ColumnInfo(name = "name") val name: String,
    @ColumnInfo(name = "profile_picture") val profilePicture: String? = null,
    @ColumnInfo(name = "office_id") val officeId: Int
)