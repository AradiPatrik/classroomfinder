package hu.aradipatrik.classroomfinder.db.daos

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import hu.aradipatrik.classroomfinder.db.entities.Classroom
import hu.aradipatrik.classroomfinder.db.pokos.ClassroomEssentials

const val DELETE_ALL_CLASSROOMS_QUERY_STRING =
        "DELETE FROM classrooms"

const val GET_ALL_CLASSROOMS_QUERY_STRING =
        "SELECT id, code, name FROM classrooms"

const val GET_CLASSROOMS_OF_BUILDING_WITH_NAME_OR_CODE_LIKE_QUERY_STRING =
        "SELECT id, code, name FROM classrooms " +
                "WHERE building_id like :buildingId and " +
                "(name like '%' || :searchQuery || '%' or code like '%' || :searchQuery || '%') " +
                "COLLATE NOCASE"

const val GET_CLASSROOMS_WITH_NAME_OR_CODE_CONTAINING_QUERY_STRING =
        "SELECT * FROM classrooms " +
                "WHERE name like '%' || :query || '%' or code like '%' || :query || '%'"

const val GET_CLASSROOM_WITH_CODE_QUERY_STRING =
        "SELECT * FROM classrooms WHERE id = :classroomId"

const val COUNT_CLASSROOMS_OF_BUILDING_WITH_CODE_LIKE =
        "SELECT COUNT(*) FROM classrooms " +
                "WHERE building_id = :buildingId and id = :classroomId"

@Dao
interface ClassroomDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(classrooms: List<Classroom>)

    @Query(GET_ALL_CLASSROOMS_QUERY_STRING)
    fun getAllClassrooms(): LiveData<List<ClassroomEssentials>>

    @Query(GET_CLASSROOMS_OF_BUILDING_WITH_NAME_OR_CODE_LIKE_QUERY_STRING)
    fun getClassroomsOfBuildingWithNameOrCodeLike(
        buildingId: Int,
        searchQuery: String
    ): LiveData<List<ClassroomEssentials>>

    @Query(GET_CLASSROOMS_WITH_NAME_OR_CODE_CONTAINING_QUERY_STRING)
    fun getClassroomsWithNameOrCodeContaining(query: String): LiveData<List<Classroom>>

    @Query(GET_CLASSROOMS_WITH_NAME_OR_CODE_CONTAINING_QUERY_STRING)
    fun getClassroomsWithNameOrCodeContainingSync(query: String): List<Classroom>

    @Query(GET_CLASSROOM_WITH_CODE_QUERY_STRING)
    fun getClassroomById(classroomId: Int): LiveData<Classroom>

    @Query(COUNT_CLASSROOMS_OF_BUILDING_WITH_CODE_LIKE)
    fun isClassroomInBuilding(buildingId: Int, classroomId: Int): LiveData<Int>

    @Query(DELETE_ALL_CLASSROOMS_QUERY_STRING)
    fun deleteAll()
}