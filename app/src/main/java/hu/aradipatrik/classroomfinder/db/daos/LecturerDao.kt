package hu.aradipatrik.classroomfinder.db.daos

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import hu.aradipatrik.classroomfinder.db.entities.Lecturer
import hu.aradipatrik.classroomfinder.db.pokos.CourseLocation

const val GET_OFFICE_OF_LECTURER_QUERY_STRING =
        "SELECT buildings.name as building_name, " +
                "lecturers.office_id as classroom_id " +
                "FROM lecturers, classrooms, buildings " +
                "WHERE lecturers.id = :id and classrooms.id = lecturers.office_id " +
                "and buildings.id = classrooms.building_id"

const val GET_LECTURERS_WITH_NAME_CONTAINING_QUERY_STRING =
        "SELECT * FROM lecturers WHERE name like '%' || :namePart || '%' COLLATE NOCASE"

const val DELETE_ALL_LECTURER_QUERY_STRING =
        "DELETE FROM lecturers"

const val GET_ALL_LECTURERS_QUERY_STRING =
        "SELECT * FROM lecturers"

@Dao
interface LecturerDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(lecturer: List<Lecturer>): Array<Long>

    @Query(GET_ALL_LECTURERS_QUERY_STRING)
    fun getAllLecturers(): LiveData<List<Lecturer>>

    @Query(GET_LECTURERS_WITH_NAME_CONTAINING_QUERY_STRING)
    fun getLecturersWithNameContaining(namePart: String): LiveData<List<Lecturer>>

    @Query(GET_LECTURERS_WITH_NAME_CONTAINING_QUERY_STRING)
    fun getLecturersWithNameContainingSync(namePart: String): List<Lecturer>

    @Query(GET_OFFICE_OF_LECTURER_QUERY_STRING)
    fun getOfficeOfLecturer(id: Int): LiveData<CourseLocation>

    @Query(DELETE_ALL_LECTURER_QUERY_STRING)
    fun deleteAll()
}