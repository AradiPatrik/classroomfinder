package hu.aradipatrik.classroomfinder.db

import hu.aradipatrik.classroomfinder.db.entities.Course

val mockCourses = listOf(
        Course(id = 0, code = "CS1", name = "Programozás 1",
                lecturerId = 1, classroomId = 5),
        Course(id = 0, code = "CS2", name = "Programozás 2",
                lecturerId = 2, classroomId = 8),
        Course(id = 0, code = "CS3", name = "Diszkrét Matematika 1",
                lecturerId = 3, classroomId = 10),
        Course(id = 0, code = "CS4", name = "Diszkrét Matematika 2",
                lecturerId = 1, classroomId = 2),
        Course(id = 0, code = "CS5", name = "Számítógépse Grafika",
                lecturerId = 1, classroomId = 20)
)