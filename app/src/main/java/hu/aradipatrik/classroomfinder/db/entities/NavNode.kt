package hu.aradipatrik.classroomfinder.db.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.ForeignKey.CASCADE
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(
        tableName = "nav_nodes",
        foreignKeys = [
            ForeignKey(
                    entity = NavNode::class,
                    parentColumns = ["id"],
                    childColumns = ["parent_id"],
                    onDelete = CASCADE
            ),
            ForeignKey(
                    entity = Building::class,
                    parentColumns = ["id"],
                    childColumns = ["building_id"],
                    onDelete = CASCADE
            )
        ],
        indices = [
            Index(value = ["parent_id"]),
            Index(value = ["building_id"])
        ]
)
data class NavNode(
    @PrimaryKey @ColumnInfo(name = "id") val id: Int,
    @ColumnInfo(name = "building_id") val buildingId: Int,
    @ColumnInfo(name = "parent_id") val parentId: Int?,
    @ColumnInfo(name = "action") val action: String,
    @ColumnInfo(name = "type") val type: String,
    @ColumnInfo(name = "image_url") val imageUrl: String
)