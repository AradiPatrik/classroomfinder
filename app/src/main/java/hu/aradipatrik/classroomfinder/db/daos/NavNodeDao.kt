package hu.aradipatrik.classroomfinder.db.daos

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import hu.aradipatrik.classroomfinder.db.entities.NavNode

const val GET_NAV_NODES_IN_BUILDING_QUERY_STRING =
        "SELECT id, building_id, parent_id, `action`, type, " +
                "image_url FROM nav_nodes WHERE building_id = :buildingId"

const val DELETE_ALL_NAVNODES_QUERY_STRING = "DELETE FROM nav_nodes"

@Dao
interface NavNodeDao {
    @Insert(onConflict = REPLACE)
    fun insertAll(courses: List<NavNode>)

    @Query(GET_NAV_NODES_IN_BUILDING_QUERY_STRING)
    fun getNavNodesInBuilding(buildingId: Int): LiveData<List<NavNode>>

    @Query(GET_NAV_NODES_IN_BUILDING_QUERY_STRING)
    fun getNavNodesInBuildingSync(buildingId: Int): List<NavNode>

    @Query(DELETE_ALL_NAVNODES_QUERY_STRING)
    fun deleteAll()
}