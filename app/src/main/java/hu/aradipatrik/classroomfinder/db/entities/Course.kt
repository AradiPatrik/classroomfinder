package hu.aradipatrik.classroomfinder.db.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.ForeignKey.CASCADE
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(
        tableName = "courses",
        foreignKeys = [
            ForeignKey(
                    entity = Classroom::class,
                    parentColumns = ["id"],
                    childColumns = ["classroom_id"],
                    onDelete = CASCADE
            ),
            ForeignKey(
                    entity = Lecturer::class,
                    parentColumns = ["id"],
                    childColumns = ["lecturer_id"],
                    onDelete = CASCADE
            )
        ],
        indices = [Index(value = ["lecturer_id"]), Index(
                value = ["classroom_id"])]
)
data class Course(
    @PrimaryKey(autoGenerate = true) @ColumnInfo(name = "id") val id: Int,
    @ColumnInfo(name = "code") val code: String,
    @ColumnInfo(name = "name") val name: String,
    @ColumnInfo(name = "lecturer_id") val lecturerId: Int,
    @ColumnInfo(name = "classroom_id") val classroomId: Int
)