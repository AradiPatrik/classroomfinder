package hu.aradipatrik.classroomfinder.db.pokos

import androidx.room.ColumnInfo

data class CourseLocation(
    @ColumnInfo(name = "building_name")
    val buildingName: String,
    @ColumnInfo(name = "classroom_id")
    val classroomId: Int
)