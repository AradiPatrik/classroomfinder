package hu.aradipatrik.classroomfinder.db.daos

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import hu.aradipatrik.classroomfinder.db.entities.Course
import hu.aradipatrik.classroomfinder.db.pokos.CourseLocation
import hu.aradipatrik.classroomfinder.db.pokos.CourseCardItem

const val DELETE_ALL_COURSE_QUERY_STRING =
        "DELETE FROM courses"

const val GET_COURSE_LOCATION_QUERY_STRING =
        "SELECT buildings.name as building_name, classroom_id " +
                "FROM courses, classrooms, buildings " +
                "WHERE courses.id = :courseId and courses.classroom_id = classrooms.id " +
                "and buildings.id = classrooms.building_id"

const val GET_ALL_COURSES_QUERY_STRING =
        "SELECT courses.id as course_id, lecturers.id as lecturer_id, courses.code as course_code, " +
                "courses.name as course_name, " +
                "lecturers.name as lecturer_name, buildings.image_url as building_image from " +
                "courses, lecturers, buildings, classrooms where " +
                "courses.lecturer_id = lecturers.id " +
                "and courses.classroom_id = classrooms.id " +
                "and classrooms.building_id = buildings.id"

@Dao
interface CourseDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(courses: List<Course>)

    @Query(DELETE_ALL_COURSE_QUERY_STRING)
    fun deleteAll()

    @Query(GET_COURSE_LOCATION_QUERY_STRING)
    fun getCourseLocation(courseId: Int): LiveData<CourseLocation>

    @Query(GET_ALL_COURSES_QUERY_STRING)
    fun getAllCourses(): LiveData<List<CourseCardItem>>
}