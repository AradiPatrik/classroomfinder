package hu.aradipatrik.classroomfinder.db

import android.util.Log
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.FirebaseDatabase
import hu.aradipatrik.classroomfinder.AppExecutors
import hu.aradipatrik.classroomfinder.db.daos.BuildingDao
import hu.aradipatrik.classroomfinder.db.daos.ClassroomDao
import hu.aradipatrik.classroomfinder.db.daos.CourseDao
import hu.aradipatrik.classroomfinder.db.daos.LecturerDao
import hu.aradipatrik.classroomfinder.db.daos.NavNodeDao
import hu.aradipatrik.classroomfinder.db.entities.Building
import hu.aradipatrik.classroomfinder.db.entities.Classroom
import hu.aradipatrik.classroomfinder.db.entities.Lecturer
import hu.aradipatrik.classroomfinder.db.entities.NavNode
import hu.aradipatrik.classroomfinder.navigation.NavTreeFactory
import hu.aradipatrik.classroomfinder.util.getValue
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.launch

class DatabaseSeeder(
        private val buildingDao: BuildingDao,
        private val classroomDao: ClassroomDao,
        private val courseDao: CourseDao,
        private val lecturerDao: LecturerDao,
        private val navNodeDao: NavNodeDao,
        private val navTreeFactory: NavTreeFactory,
        private val appExecutors: AppExecutors
) {
    lateinit var buildings: List<Building>
    lateinit var classrooms: List<Classroom>
    lateinit var lecturers: List<Lecturer>
    lateinit var navNodes: List<NavNode>

    suspend fun seedDatabase() {
        deleteAllEntitiesFromEveryTable()
        insertNullObjectsForForeignKeyConstraints()
        getFirebaseDatabase()
        insertEntitiesReceivedFromFirebase()
    }

    private fun insertEntitiesReceivedFromFirebase() {
        buildingDao.insertAll(buildings)
        navNodeDao.insertAll(navTreeFactory
                .createFromNavNodeList(navNodes)
                .getDepthFirstOrdering())
        classroomDao.insertAll(classrooms)
        lecturerDao.insertAll(lecturers)
        courseDao.insertAll(mockCourses)
    }

    private suspend fun getFirebaseDatabase() = coroutineScope {
        launch(Dispatchers.Default) { buildings = getBuildings() }
        launch(Dispatchers.Default) { classrooms = getClassrooms() }
        launch(Dispatchers.Default) { lecturers = getLecturers() }
        launch(Dispatchers.Default) { navNodes = getNavNodes() }
        return@coroutineScope
    }

    private fun insertNullObjectsForForeignKeyConstraints() {
        buildingDao.insertAll(listOf(nonExistentBuilding))
        navNodeDao.insertAll(listOf(nonExistentNavNode))
        classroomDao.insertAll(listOf(nonExistentClassroom))
    }

    private fun deleteAllEntitiesFromEveryTable() {
        buildingDao.deleteAll()
        classroomDao.deleteAll()
        courseDao.deleteAll()
        lecturerDao.deleteAll()
        navNodeDao.deleteAll()
    }

    private suspend fun <T> getListFromFirebase(
            path: String,
            mapper: (DataSnapshot) -> T
    ) =
            FirebaseDatabase
                    .getInstance()
                    .getReference(path)
                    .getValue()
                    .children
                    .map { mapper(it) }

    private suspend fun getNavNodes() =
            getListFromFirebase("navNodes") {
                navNodeFromSnapshot(it)
            }

    private suspend fun getBuildings() =
            getListFromFirebase("buildings") {
                buildingFromSnapshot(it)
            }

    private fun buildingFromSnapshot(it: DataSnapshot) = Building(
            id = it.key.toString().toInt(),
            imageUrl = it.child("imageUrl").value.toString(),
            name = it.child("name").value.toString(),
            streetName = it.child("streetName").value.toString()
    )

    private suspend fun getClassrooms() =
            getListFromFirebase("classrooms") {
                classroomFromSnapshot(it)
            }

    private fun classroomFromSnapshot(it: DataSnapshot) = Classroom(
            id = it.key.toString().toInt(),
            code = it.child("code").value.toString(),
            name = it.child("name").value.toString(),
            buildingId = it.child("buildingId").value.toString().toInt(),
            navNodeId = it.child("navNodeId").value.toString().toInt(),
            position = it.child("position").value.toString().toInt(),
            direction = it.child("direction").value.toString()
    )

    private suspend fun getLecturers() =
            getListFromFirebase("lecturer") {
                lecturerFromSnapshot(it)
            }

    private fun lecturerFromSnapshot(it: DataSnapshot) = Lecturer(
            id = it.key.toString().toInt(),
            code = it.child("code").value.toString(),
            name = it.child("name").value.toString(),
            officeId = it.child("officeId").value.toString().toInt(),
            email = it.child("email").value.toString()
    )

    private fun navNodeFromSnapshot(it: DataSnapshot) = NavNode(
            id = it.key.toString().toInt(),
            buildingId = it.child("buildingId").value.toString().toInt(),
            parentId = if (it.child("parentId").value.toString().toInt() == 0) {
                null
            } else {
                it.child("parentId").value.toString().toInt()
            },
            action = it.child("action").value.toString(),
            type = it.child("type").value.toString(),
            imageUrl = if (it.hasChild("image_url")) {
                it.child("image_url").value.toString()
            } else {
                NAV_NODE_NO_IMAGE
            }
    )

    private fun <T> logList(message: String, list: List<T>) {
        Log.d("APDEBUG", message)
        list.forEach { Log.d("APDEBUG", it.toString()) }
    }
}