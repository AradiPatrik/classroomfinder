package hu.aradipatrik.classroomfinder.db.daos

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import hu.aradipatrik.classroomfinder.db.NON_EXISTENT_BUILDING_ID
import hu.aradipatrik.classroomfinder.db.entities.Building

const val GET_ALL_BUILDINGS_QUERY_STRING =
        "SELECT * FROM buildings WHERE id != $NON_EXISTENT_BUILDING_ID"

const val GET_BUILDING_BY_NAME_QUERY_STRING =
        "SELECT * FROM buildings WHERE name like '%' || :buildingName || '%' COLLATE NOCASE"

const val DELETE_ALL_BUILDINGS_QUERY_STRING =
        "DELETE FROM buildings"

const val GET_BUILDING_BY_ID_QUERY_STRING =
        "SELECT * FROM buildings WHERE :id like id"

@Dao
interface BuildingDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(classrooms: List<Building>)

    @Query(GET_ALL_BUILDINGS_QUERY_STRING)
    fun getAllBuildings(): LiveData<List<Building>>

    @Query(GET_BUILDING_BY_NAME_QUERY_STRING)
    fun getBuildingsFilteredByNameSync(buildingName: String): List<Building>

    @Query(GET_BUILDING_BY_ID_QUERY_STRING)
    fun getBuildingById(id: Int): LiveData<Building>

    @Query(DELETE_ALL_BUILDINGS_QUERY_STRING)
    fun deleteAll()
}