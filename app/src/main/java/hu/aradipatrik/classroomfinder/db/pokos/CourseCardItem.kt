package hu.aradipatrik.classroomfinder.db.pokos

import androidx.room.ColumnInfo

data class CourseCardItem(
    @ColumnInfo(name = "course_id")
    val courseId: Int,
    @ColumnInfo(name = "lecturer_id")
    val lecturerId: Int,
    @ColumnInfo(name = "course_code")
    val courseCode: String,
    @ColumnInfo(name = "course_name")
    val courseName: String,
    @ColumnInfo(name = "lecturer_name")
    val lecturerName: String,
    @ColumnInfo(name = "building_image")
    val buildingImage: String
)