package hu.aradipatrik.classroomfinder.db

import hu.aradipatrik.classroomfinder.db.entities.Building
import hu.aradipatrik.classroomfinder.db.entities.Classroom
import hu.aradipatrik.classroomfinder.db.entities.NavNode

internal const val DATABASE_NAME = "app_database"
internal const val CHANNEL_ID = "Aradi Patrik Notification Channel"
internal const val NAV_NODE_NO_IMAGE = "https://firebasestorage.googleapis.com/v0/b/szteclassroomfinder.appspot.com/o/nav_node_pictures%2Fhallway.jpg?alt=media&token=e27564d5-0623-4e3d-8e21-122d7c777ae2"
internal const val AUTO_GENERATE_ID = 0
internal const val NON_EXISTENT_CLASSROOM_ID = Int.MAX_VALUE
internal const val NON_EXISTENT_BUILDING_ID = Int.MAX_VALUE
internal const val NON_EXISTENT_NAV_NODE_ID = Int.MAX_VALUE

internal val nonExistentClassroom = Classroom(
        id = NON_EXISTENT_CLASSROOM_ID,
        code = "",
        name = "",
        buildingId = NON_EXISTENT_BUILDING_ID,
        navNodeId = NON_EXISTENT_NAV_NODE_ID,
        position = 0,
        direction = ""
)

internal val nonExistentBuilding = Building(
        id = NON_EXISTENT_BUILDING_ID,
        name = "",
        imageUrl = null,
        streetName = null
)

internal val nonExistentNavNode = NavNode(
        id = NON_EXISTENT_NAV_NODE_ID,
        buildingId = NON_EXISTENT_BUILDING_ID,
        parentId = null,
        action = "",
        type = "",
        imageUrl = ""
)
