package hu.aradipatrik.classroomfinder.db.pokos

import hu.aradipatrik.classroomfinder.db.entities.NavNode
import hu.aradipatrik.classroomfinder.service.ActionTranslatorService

data class CardStackItemText(val heading: String, val subText: String)

data class NavNodeCardStackItem(
    val id: Int,
    val directionInfo: CardStackItemText,
    val type: String,
    val imageUrl: String
) {
    companion object {
        fun fromNavNodePair(
            navNodePair: Pair<NavNode, NavNode>,
            actionTranslatorService: ActionTranslatorService
        ) =
                NavNodeCardStackItem(
                        id = navNodePair.first.id,
                        directionInfo = actionTranslatorService.translateTurnAction(
                                action = navNodePair.second.action,
                                type = navNodePair.first.type
                        ),
                        type = navNodePair.first.type,
                        imageUrl = navNodePair.first.imageUrl
                )
    }
}