package hu.aradipatrik.classroomfinder.db.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.ForeignKey.CASCADE
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(
        tableName = "classrooms",
        foreignKeys = [
            ForeignKey(
                    entity = Building::class,
                    parentColumns = ["id"],
                    childColumns = ["building_id"],
                    onDelete = CASCADE
            ),
            ForeignKey(
                entity = NavNode::class,
                    parentColumns = ["id"],
                    childColumns = ["nav_node_id"],
                    onDelete = CASCADE
            )
        ],
        indices = [
            Index(value = ["building_id"]),
            Index(value = ["nav_node_id"])
        ]
)
data class Classroom(
    @PrimaryKey @ColumnInfo(name = "id") val id: Int,
    @ColumnInfo(name = "code") val code: String,
    @ColumnInfo(name = "name") val name: String? = null,
    @ColumnInfo(name = "building_id") val buildingId: Int,
    @ColumnInfo(name = "nav_node_id") val navNodeId: Int,
    @ColumnInfo(name = "position") val position: Int,
    @ColumnInfo(name = "direction") val direction: String
)