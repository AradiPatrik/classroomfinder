package hu.aradipatrik.classroomfinder

import android.app.NotificationChannel
import android.app.NotificationManager
import android.os.Build
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.ui.NavigationUI
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.bottomnavigation.BottomNavigationView
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import hu.aradipatrik.classroomfinder.db.CHANNEL_ID
import hu.aradipatrik.classroomfinder.databinding.ActivityMainBinding
import hu.aradipatrik.classroomfinder.view.custom.BottomNavigationBehavior
import hu.aradipatrik.classroomfinder.view.custom.SearchViewWrapper
import hu.aradipatrik.classroomfinder.view.custom.SearchViewWrapperImpl
import javax.inject.Inject

interface FabManager {
    fun setFabVisible(visible: Boolean)
    fun setFabIcon(resourceId: Int)
    fun setFabClickListener(f: () -> Unit)
}

interface AppBorderManager {
    fun showNavAndToolBar()
}

interface SearchViewOwner {
    fun getSearchView(): SearchViewWrapper
}

class MainActivity :
        AppCompatActivity(), FabManager, AppBorderManager,
        HasSupportFragmentInjector, SearchViewOwner {
    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: ActivityMainBinding
    private lateinit var navController: NavController
    private lateinit var searchViewWrapper: SearchViewWrapper

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        createNotificationChannel()

        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        navController = Navigation.findNavController(this, R.id.navHostFragment)

        setSupportActionBar(binding.toolbar)
        NavigationUI.setupActionBarWithNavController(this, navController)

        binding.bottomNavView.setupWithNavController(navController)

        searchViewWrapper = SearchViewWrapperImpl(binding.searchView)

        hideBackButtonOnTopLevelDestinations()
    }

    override fun onSupportNavigateUp() =
            navController.navigateUp()

    private fun hideBackButtonOnTopLevelDestinations() {
        navController.addOnDestinationChangedListener { _, destination, _ ->
            supportActionBar?.setDisplayShowHomeEnabled(
                    !isTopLevelFragment(destination.id))
            supportActionBar?.setDisplayHomeAsUpEnabled(
                    !isTopLevelFragment(destination.id))
        }
    }

    private fun isTopLevelFragment(id: Int) =
            id == R.id.dashboardFragment ||
                    id == R.id.chooseBuildingFragment

    override fun setFabVisible(visible: Boolean) = if (visible) {
        binding.addFab.show()
    } else {
        binding.addFab.hide()
    }

    override fun setFabClickListener(f: () -> Unit) {
        binding.addFab.setOnClickListener { f() }
    }

    override fun setFabIcon(resourceId: Int) {
        binding.addFab.setImageResource(resourceId)
        binding.addFab.hide()
        binding.addFab.show()
    }

    override fun showNavAndToolBar() {
        val appBarLayout = findViewById<AppBarLayout>(R.id.appbar_layout)
        appBarLayout.setExpanded(true, true)

        val bottomNavBar = findViewById<BottomNavigationView>(R.id.bottom_nav_view)
        val layoutParams = bottomNavBar.layoutParams as CoordinatorLayout.LayoutParams
        (layoutParams.behavior as BottomNavigationBehavior).expand()
    }

    override fun getSearchView() = searchViewWrapper

    private fun createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = getString(R.string.channel_name)
            val description = getString(R.string.channel_description)
            val importance = NotificationManager.IMPORTANCE_HIGH
            val channel = NotificationChannel(CHANNEL_ID, name, importance)
            channel.description = description
            val notificationManager = getSystemService(NotificationManager::class.java)
            notificationManager!!.createNotificationChannel(channel)
        }
    }

    override fun supportFragmentInjector(): AndroidInjector<Fragment> =
            dispatchingAndroidInjector
}
