package hu.aradipatrik.classroomfinder.ui.buildings

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import hu.aradipatrik.classroomfinder.repository.BuildingRepository
import hu.aradipatrik.classroomfinder.db.entities.Building
import hu.aradipatrik.classroomfinder.testing.OpenForTesting
import javax.inject.Inject

@OpenForTesting
class ChooseBuildingViewModel @Inject constructor(
    buildingRepository: BuildingRepository
) : ViewModel() {
    val buildings: LiveData<List<Building>> = buildingRepository.allBuildings
}