package hu.aradipatrik.classroomfinder.ui.createcourse

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Filter

open class AutocompleteListAdapter<T>(
    context: Context,
    private val resourceId: Int,
    private val viewBinder: (dataToBind: T, view: View) -> Unit,
    private val valueFilter: (constraint: CharSequence) -> List<T>,
    private val toStringMapper: (item: T) -> CharSequence,
    private var dataList: MutableList<T>? = ArrayList()
)
    : ArrayAdapter<T>(context, resourceId, dataList!!) {

    private val listFilter = ListFilter()

    override fun getCount() = dataList!!.size

    override fun getItem(position: Int): T {
        return dataList!![position]
    }

    override fun getView(position: Int, view: View?, parent: ViewGroup): View {
        val currentView = view ?: LayoutInflater.from(parent.context)
                .inflate(resourceId, parent, false)

        currentView?.let {
            viewBinder(dataList!![position], currentView)
        }

        return currentView!!
    }

    override fun getFilter(): Filter {
        return listFilter
    }

    inner class ListFilter : Filter() {
        private val lock = Object()

        override fun performFiltering(constraint: CharSequence?): FilterResults {
            val results = FilterResults()
            if (constraint == null || constraint.isEmpty()) {
                synchronized(lock) {
                    results.values = ArrayList<T>()
                    results.count = 0
                }
            } else {
                val matchedValues = valueFilter(constraint)
                results.values = matchedValues
                results.count = matchedValues.size
            }

            return results
        }

        @Suppress("UNCHECKED_CAST")
        override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
            results?.let {
                dataList = if (it.values != null) {
                    results.values as ArrayList<T>
                } else {
                    null
                }

                if (it.count > 0) {
                    notifyDataSetChanged()
                } else {
                    notifyDataSetInvalidated()
                }
            }
        }

        @Suppress("UNCHECKED_CAST")
        override fun convertResultToString(resultValue: Any?) =
                toStringMapper(resultValue as T)
    }
}
