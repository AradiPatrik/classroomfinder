package hu.aradipatrik.classroomfinder.ui.dashboard

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.NavHostFragment
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.input.getInputField
import com.afollestad.materialdialogs.input.input
import com.google.android.material.snackbar.Snackbar
import hu.aradipatrik.classroomfinder.AppExecutors
import hu.aradipatrik.classroomfinder.FabManager
import hu.aradipatrik.classroomfinder.R
import hu.aradipatrik.classroomfinder.db.pokos.CourseCardItem
import hu.aradipatrik.classroomfinder.databinding.FragmentDashboardBinding
import hu.aradipatrik.classroomfinder.db.NON_EXISTENT_CLASSROOM_ID
import hu.aradipatrik.classroomfinder.injection.Injectable
import hu.aradipatrik.classroomfinder.service.DialogFactory
import hu.aradipatrik.classroomfinder.service.GoogleMapsService
import hu.aradipatrik.classroomfinder.service.ImageLoaderService
import hu.aradipatrik.classroomfinder.testing.OpenForTesting
import javax.inject.Inject

/**
 * The UI Controller for displaying the users courses with their actions,
 * which contain navigating either to the course or to the lecturer of the course
 */
@OpenForTesting
class DashboardFragment : Fragment(), Injectable {
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    lateinit var viewModel: DashboardViewModel

    @Inject
    lateinit var fabManager: FabManager

    @Inject
    lateinit var googleMapsService: GoogleMapsService

    @Inject
    lateinit var imageLoaderService: ImageLoaderService

    @Inject
    lateinit var appExecutors: AppExecutors

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        val binding = FragmentDashboardBinding.inflate(inflater, container, false)
        viewModel = ViewModelProviders
                .of(this, viewModelFactory)
                .get(DashboardViewModel::class.java)

        setHasOptionsMenu(true)
        setupFab()

        val adapter = CourseAdapter(
                appExecutors,
                imageLoaderService,
                onToCourseClick = this::onNavigateToCourseButtonClick,
                onToLecturerClick = this::onNavigateToLecturerButtonClick)

        binding.recyclerView.adapter = adapter
        subscribeUi(adapter)
        subscribeToImport(binding)

        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.menu_dashboard, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.importFromNeptun ->
                MaterialDialog(activity!!).show {
                    message(R.string.neptun_import_dialog_body)
                    input(waitForPositiveButton = true) { materialDialog, charSequence ->
                        viewModel.importClassesFromNeptun(charSequence.toString())
                    }
                    positiveButton(R.string.neptun_import_dialog_positive_button)
                    negativeButton(R.string.neptun_import_dialog_negative_button)
                    getInputField().setTextIsSelectable(true)
                    title(R.string.neptun_import_dialog_title)
                }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun setupFab() {
        fabManager.setFabVisible(true)
        fabManager.setFabIcon(R.drawable.ic_add_white)
        fabManager.setFabClickListener {
            navController().navigate(R.id.action_dashboardFragment_to_createCourseFragment)
        }
    }

    private fun subscribeUi(adapter: CourseAdapter) {
        viewModel.allCourses.observe(viewLifecycleOwner, Observer { courses ->
            if (courses != null) adapter.submitList(courses)
        })
    }

    private fun subscribeToImport(binding: FragmentDashboardBinding) {
        viewModel.badImportCount.observe(viewLifecycleOwner, Observer {
            val snack = Snackbar.make(
                    binding.root,
                    "Done! (unsuccessful import count: $it)",
                    Snackbar.LENGTH_INDEFINITE
            )
            snack.setAction("OK") { snack.dismiss() }
            snack.show()
        })
    }

    private fun onNavigateToCourseButtonClick(courseCardItem: CourseCardItem) {
        viewModel.getCourseLocation(courseCardItem).observe(
                viewLifecycleOwner,
                Observer { location ->
                    if (location != null) {
                        val classroomId = location.classroomId
                        val buildingName = location.buildingName
                        startNavigation(buildingName, classroomId)
                    }
                })
    }

    private fun onNavigateToLecturerButtonClick(courseCardItem: CourseCardItem) {
        viewModel.getLecturerOfficeOfCourse(courseCardItem).observe(
                viewLifecycleOwner,
                Observer { office ->
                    if (office != null && office.classroomId != NON_EXISTENT_CLASSROOM_ID) {
                        val classroomId = office.classroomId
                        val buildingName = office.buildingName
                        startNavigation(buildingName, classroomId)
                    }
                })
    }

    private fun startNavigation(buildingName: String, classroomId: Int) {
        googleMapsService.navigateTo(buildingName)
        navController().navigate(DashboardFragmentDirections.navigateToClassroom(classroomId))
    }

    fun navController() = NavHostFragment.findNavController(this)
}
