package hu.aradipatrik.classroomfinder.ui.classrooms

import androidx.recyclerview.widget.DiffUtil
import hu.aradipatrik.classroomfinder.db.pokos.ClassroomEssentials

class ClassroomDiffCallback : DiffUtil.ItemCallback<ClassroomEssentials>() {
    override fun areItemsTheSame(
        oldItem: ClassroomEssentials,
        newItem: ClassroomEssentials
    ) =
            oldItem.code == newItem.code

    override fun areContentsTheSame(
        oldItem: ClassroomEssentials,
        newItem: ClassroomEssentials
    ) =
            oldItem == newItem
}