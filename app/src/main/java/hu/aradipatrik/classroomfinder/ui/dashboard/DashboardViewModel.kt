package hu.aradipatrik.classroomfinder.ui.dashboard

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import hu.aradipatrik.classroomfinder.db.AUTO_GENERATE_ID
import hu.aradipatrik.classroomfinder.db.entities.Course
import hu.aradipatrik.classroomfinder.db.pokos.CourseCardItem
import hu.aradipatrik.classroomfinder.db.pokos.CourseLocation
import hu.aradipatrik.classroomfinder.repository.ClassroomRepository
import hu.aradipatrik.classroomfinder.repository.CourseRepository
import hu.aradipatrik.classroomfinder.repository.LecturerRepository
import hu.aradipatrik.classroomfinder.service.CalendarParserService
import hu.aradipatrik.classroomfinder.service.HttpService
import hu.aradipatrik.classroomfinder.testing.OpenForTesting
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import javax.inject.Inject

@OpenForTesting
class DashboardViewModel @Inject constructor(
        private val courseRepository: CourseRepository,
        private val lecturerRepository: LecturerRepository,
        private val classroomRepository: ClassroomRepository,
        private val httpService: HttpService,
        private val calendarParserService: CalendarParserService
) : ViewModel() {
    val allCourses: LiveData<List<CourseCardItem>> = courseRepository.allCourses
    private var _badImportCount = MutableLiveData<Int>()
    val badImportCount: LiveData<Int> = _badImportCount


    fun getCourseLocation(course: CourseCardItem) =
            courseRepository.getCourseLocation(course.courseId)

    fun getLecturerOfficeOfCourse(course: CourseCardItem): LiveData<CourseLocation> {
        return lecturerRepository.getOfficeOfLecturer(course.lecturerId)
    }

    fun importClassesFromNeptun(importUrl: String) = GlobalScope.launch {
        val calendarText = httpService.getString(importUrl);
        val calendarCourseList = calendarParserService.parse(calendarText)
        var unsuccessfulImportCount = 0
        calendarCourseList.forEach {
            val matchedLecturers = lecturerRepository
                    .getLecturersWithNameContainingSync(it.lecturerName)
            val matchedClassrooms = classroomRepository
                    .getClassroomsWithNameOrCodeContainingSync(it.classroomCode)
            if (matchedLecturers.size == 1 && matchedClassrooms.size == 1) {
                courseRepository.insertCourse(Course(
                        AUTO_GENERATE_ID,
                        it.courseCode,
                        it.courseName,
                        matchedLecturers[0].id,
                        matchedClassrooms[0].id
                ))
            } else {
                unsuccessfulImportCount++
            }
        }
        if (unsuccessfulImportCount > 0) {
            _badImportCount.postValue(unsuccessfulImportCount)
        }
    }
}
