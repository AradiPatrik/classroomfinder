package hu.aradipatrik.classroomfinder.ui.navigation

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import hu.aradipatrik.classroomfinder.repository.ClassroomRepository
import hu.aradipatrik.classroomfinder.repository.NavNodeRepository
import hu.aradipatrik.classroomfinder.db.entities.Classroom
import hu.aradipatrik.classroomfinder.db.entities.NavNode
import hu.aradipatrik.classroomfinder.db.pokos.NavNodeCardStackItem
import hu.aradipatrik.classroomfinder.navigation.NavTreeFactory
import hu.aradipatrik.classroomfinder.service.ActionTranslatorService
import javax.inject.Inject

class NavigationViewModel @Inject constructor(
        private val classroomRepository: ClassroomRepository,
        private val navNodeRepository: NavNodeRepository,
        private val actionTranslatorService: ActionTranslatorService,
        private val navTreeFactory: NavTreeFactory
) : ViewModel() {
    fun getRouteToClassroom(classroomId: Int): LiveData<List<NavNodeCardStackItem>> {
        var classroom: Classroom? = null
        val navNodesLiveData = Transformations
                .switchMap(classroomRepository.getClassroomById(classroomId)) {
                    classroom = it
                    navNodeRepository.getNavNodesOfBuilding(it.buildingId)
                }

        return Transformations.map(navNodesLiveData) { navNodes ->
            mapNavNodesToNavNodeCardStackItems(
                    navTreeFactory.createFromNavNodeList(navNodes)
                            .getRouteToNode(classroom?.navNodeId!!), classroom!!)
        }
    }

    private fun mapNavNodesToNavNodeCardStackItems(
        navNodes: List<NavNode>,
        classroom: Classroom
    ): List<NavNodeCardStackItem> {
        val cardStackItems = ArrayList<NavNodeCardStackItem>(navNodes.size)
        downShiftActions(navNodes, cardStackItems)
        addLastItemToCardStack(navNodes, cardStackItems, classroom)
        return cardStackItems
    }

    private fun downShiftActions(
        navNodes: List<NavNode>,
        cardStackItems: ArrayList<NavNodeCardStackItem>
    ) {
        for (i in 0 until navNodes.size - 1) {
            cardStackItems.add(NavNodeCardStackItem
                    .fromNavNodePair(
                            Pair(navNodes[i], navNodes[i + 1]),
                            actionTranslatorService
                    )
            )
        }
    }

    private fun addLastItemToCardStack(
        navNodes: List<NavNode>,
        cardStackItems: ArrayList<NavNodeCardStackItem>,
        classroom: Classroom
    ) {
        navNodes.last().apply {
            cardStackItems.add(
                    NavNodeCardStackItem(
                            id = id,
                            directionInfo = actionTranslatorService.translateDestinationReached(classroom),
                            type = type,
                            imageUrl = imageUrl
                    ))
        }
    }
}