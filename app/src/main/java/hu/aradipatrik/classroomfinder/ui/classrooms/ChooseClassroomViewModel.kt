package hu.aradipatrik.classroomfinder.ui.classrooms

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import hu.aradipatrik.classroomfinder.repository.ClassroomRepository
import hu.aradipatrik.classroomfinder.db.pokos.ClassroomEssentials
import hu.aradipatrik.classroomfinder.testing.OpenForTesting
import hu.aradipatrik.classroomfinder.util.livedata.DoubleTrigger
import javax.inject.Inject

@OpenForTesting
class ChooseClassroomViewModel @Inject constructor(
        private val classroomRepository: ClassroomRepository
) : ViewModel() {
    private lateinit var buildingName: String
    private val buildingId = MutableLiveData<Int>()
    private val searchQuery = MutableLiveData<String>()
    private val filter = DoubleTrigger<Int, String>(buildingId, searchQuery)
    val classrooms: LiveData<List<ClassroomEssentials>?> =
            Transformations.switchMap(filter) {
                val buildingId = it.first
                val query = it.second
                return@switchMap if (buildingId != null) {
                    classroomRepository.getAllClassroomsOfBuildingFiltered(
                            buildingId, query ?: "")
                } else {
                    MutableLiveData<List<ClassroomEssentials>>()
                }
            }

    fun setBuilding(buildingName: String, buildingId: Int) {
        this.buildingId.postValue(buildingId)
        this.buildingName = buildingName
    }

    fun getBuildingName() = buildingName

    fun queryClassrooms(query: String) {
        searchQuery.postValue(query)
    }
}
