package hu.aradipatrik.classroomfinder.ui.navigation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.DecelerateInterpolator
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.yuyakaido.android.cardstackview.CardStackLayoutManager
import com.yuyakaido.android.cardstackview.CardStackListener
import com.yuyakaido.android.cardstackview.CardStackView
import com.yuyakaido.android.cardstackview.Direction
import com.yuyakaido.android.cardstackview.RewindAnimationSetting
import hu.aradipatrik.classroomfinder.AppBorderManager
import hu.aradipatrik.classroomfinder.AppExecutors
import hu.aradipatrik.classroomfinder.FabManager
import hu.aradipatrik.classroomfinder.R
import hu.aradipatrik.classroomfinder.databinding.FragmentInBuildingNavigationBinding
import hu.aradipatrik.classroomfinder.injection.Injectable
import hu.aradipatrik.classroomfinder.service.NotificationService
import javax.inject.Inject

/**
 * The UI Controller for displaying the route to the sought classroom, with a card stack
 */
class InBuildingNavigationFragment : Fragment(), Injectable {
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var fabManager: FabManager

    @Inject
    lateinit var appBorderManager: AppBorderManager

    @Inject
    lateinit var notificationService: NotificationService

    @Inject
    lateinit var appExecutors: AppExecutors

    private lateinit var binding: FragmentInBuildingNavigationBinding
    private var classroomId = 0
    private val manager by lazy { CardStackLayoutManager(activity!!, CardStackListener.DEFAULT) }
    lateinit var viewModel: NavigationViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentInBuildingNavigationBinding.inflate(inflater)

        classroomId = InBuildingNavigationFragmentArgs.fromBundle(arguments!!).classroomCode
        viewModel = ViewModelProviders
                .of(this, viewModelFactory)
                .get(NavigationViewModel::class.java)

        val cardStackAdapter = NavNodeAdapter(appExecutors)
        val cardStackView = binding.cardStackView

        cardStackView.layoutManager = CardStackLayoutManager(activity!!)
        cardStackView.adapter = cardStackAdapter
        cardStackView.isNestedScrollingEnabled = false

        if (deviceIsNotRotated(savedInstanceState)) {
            appBorderManager.showNavAndToolBar()
        }

        setupFab(cardStackView)

        subscribeUi(cardStackAdapter)

        return binding.root
    }

    private fun deviceIsNotRotated(savedInstanceState: Bundle?) = savedInstanceState == null

    override fun onResume() {
        super.onResume()
        notificationService.removeBackToAppNotification()
    }

    private fun setupFab(cardStackView: CardStackView) {
        fabManager.setFabVisible(true)
        fabManager.setFabClickListener {
            val setting = RewindAnimationSetting.Builder()
                    .setDirection(Direction.Bottom)
                    .setDuration(200)
                    .setInterpolator(DecelerateInterpolator())
                    .build()
            manager.setRewindAnimationSetting(setting)
            cardStackView.rewind()
        }
        fabManager.setFabIcon(R.drawable.ic_refresh_white)
    }

    private fun subscribeUi(adapter: NavNodeAdapter) {
        viewModel.getRouteToClassroom(classroomId)
                .observe(viewLifecycleOwner, Observer { nodes ->
                    adapter.submitList(nodes)
                })
    }
}
