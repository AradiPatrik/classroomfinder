package hu.aradipatrik.classroomfinder.ui.buildings

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.AsyncDifferConfig
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import hu.aradipatrik.classroomfinder.AppExecutors
import hu.aradipatrik.classroomfinder.R
import hu.aradipatrik.classroomfinder.db.entities.Building
import hu.aradipatrik.classroomfinder.databinding.BuildingCardBinding
import hu.aradipatrik.classroomfinder.service.ImageLoaderService

class BuildingAdapter(
    appExecutors: AppExecutors,
    private val imageLoaderService: ImageLoaderService,
    private val onListClassroomsButtonClick: (building: Building) -> Unit,
    private val onNavigateButtonClick: (building: Building) -> Unit
) : ListAdapter<Building, BuildingAdapter.ViewHolder>(AsyncDifferConfig.Builder(BuildingDiffCallback())
        .setBackgroundThreadExecutor(appExecutors.diskIO())
        .build()) {
    override fun onCreateViewHolder(parent: ViewGroup, position: Int) =
            ViewHolder(
                    DataBindingUtil.inflate(
                            LayoutInflater.from(parent.context),
                            R.layout.building_card, parent, false
                    ), imageLoaderService, onListClassroomsButtonClick, onNavigateButtonClick
            )

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val building = getItem(position)
        holder.itemView.tag = building
        holder.bind(building)
    }

    class ViewHolder(
        private val binding: BuildingCardBinding,
        private val imageLoaderService: ImageLoaderService,
        private val onListClassroomsButtonClick: (building: Building) -> Unit,
        private val onNavigateButtonClick: (building: Building) -> Unit
    ) :
            RecyclerView.ViewHolder(binding.root) {

        fun bind(building: Building) {
            binding.apply {
                buildingName.text = building.name
                streetName.text = building.streetName
                imageLoaderService.loadImageInto(
                        imageUrl = building.imageUrl!!, imageView = buildingImage)

                classroomsButton.setOnClickListener {
                    onListClassroomsButtonClick(building)
                }

                navigateButton.setOnClickListener {
                    onNavigateButtonClick(building)
                }

                executePendingBindings()
            }
        }
    }
}