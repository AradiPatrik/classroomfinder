package hu.aradipatrik.classroomfinder.ui.navigation

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.AsyncDifferConfig
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import hu.aradipatrik.classroomfinder.AppExecutors
import hu.aradipatrik.classroomfinder.R
import hu.aradipatrik.classroomfinder.db.pokos.NavNodeCardStackItem
import hu.aradipatrik.classroomfinder.databinding.NavNodeItemBinding

class NavNodeAdapter(appExecutors: AppExecutors) :
    ListAdapter<NavNodeCardStackItem, NavNodeAdapter.ViewHolder>(
        AsyncDifferConfig.Builder(NavNodeCardStackItemDiffCallback())
            .setBackgroundThreadExecutor(appExecutors.diskIO())
            .build()
    ) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
            ViewHolder(
                    DataBindingUtil.inflate(
                            LayoutInflater.from(parent.context),
                            R.layout.nav_node_item, parent, false
                    )
            )

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val navNode = getItem(position)
        holder.itemView.tag = navNode
        holder.bind(navNode)
    }

    class ViewHolder(val binding: NavNodeItemBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(navNode: NavNodeCardStackItem) {
            binding.apply {
                Picasso.get().load(navNode.imageUrl).into(itemImage)
                heading.text = navNode.directionInfo.heading.capitalize()
                subText.text = navNode.directionInfo.subText.capitalize()
                executePendingBindings()
            }
        }
    }
}