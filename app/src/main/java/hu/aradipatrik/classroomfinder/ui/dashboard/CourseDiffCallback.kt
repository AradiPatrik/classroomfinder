package hu.aradipatrik.classroomfinder.ui.dashboard

import androidx.recyclerview.widget.DiffUtil
import hu.aradipatrik.classroomfinder.db.pokos.CourseCardItem

class CourseDiffCallback : DiffUtil.ItemCallback<CourseCardItem>() {
    override fun areItemsTheSame(
        oldItem: CourseCardItem,
        newItem: CourseCardItem
    ) =
            oldItem.courseName == newItem.courseName

    override fun areContentsTheSame(
        oldItem: CourseCardItem,
        newItem: CourseCardItem
    ) =
            oldItem == newItem
}