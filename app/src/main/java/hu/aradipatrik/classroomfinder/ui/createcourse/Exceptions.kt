package hu.aradipatrik.classroomfinder.ui.createcourse

import hu.aradipatrik.classroomfinder.R

sealed class ExceptionWithMessage(val messageResourceId: Int): Exception()

class EmptyCourseCodeException(messageResourceId: Int = R.string.empty_course_code)
    : ExceptionWithMessage(messageResourceId)

open class EmptyCourseNameException(messageResourceId: Int = R.string.empty_course_name)
    : ExceptionWithMessage(messageResourceId)

class NoSuchBuildingException(messageResourceId: Int = R.string.no_such_building)
    : ExceptionWithMessage(messageResourceId)

class NoSuchClassroomException(messageResourceId: Int = R.string.no_such_classroom)
    : ExceptionWithMessage(messageResourceId)

class NoSuchLecturerException(messageResourceId: Int = R.string.no_such_lecturer)
    : ExceptionWithMessage(messageResourceId)