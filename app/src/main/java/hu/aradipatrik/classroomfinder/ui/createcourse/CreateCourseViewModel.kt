package hu.aradipatrik.classroomfinder.ui.createcourse

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import hu.aradipatrik.classroomfinder.AppExecutors
import hu.aradipatrik.classroomfinder.db.AUTO_GENERATE_ID
import hu.aradipatrik.classroomfinder.db.entities.Building
import hu.aradipatrik.classroomfinder.db.entities.Classroom
import hu.aradipatrik.classroomfinder.db.entities.Course
import hu.aradipatrik.classroomfinder.db.entities.Lecturer
import hu.aradipatrik.classroomfinder.repository.BuildingRepository
import hu.aradipatrik.classroomfinder.repository.ClassroomRepository
import hu.aradipatrik.classroomfinder.repository.CourseRepository
import hu.aradipatrik.classroomfinder.repository.LecturerRepository
import javax.inject.Inject

class CreateCourseViewModel @Inject constructor(
        private val lecturerRepository: LecturerRepository,
        private val buildingRepository: BuildingRepository,
        private val classroomRepository: ClassroomRepository,
        private val courseRepository: CourseRepository,
        private val appExecutors: AppExecutors
) : ViewModel() {

    fun createCourse(course: Course) =
            appExecutors.diskIO().execute { courseRepository.insertCourse(course) }

    fun createCourseWithUnknownLecturer(
            courseName: String,
            courseCode: String,
            classroomId: Int,
            lecturerName: String
    ) = appExecutors.diskIO().execute {
        val lecturerId = lecturerRepository.insertLecturerWithoutOffice(lecturerName)[0]
        courseRepository.insertCourse(Course(
                id = AUTO_GENERATE_ID,
                name = courseName,
                code = courseCode,
                lecturerId = lecturerId.toInt(),
                classroomId = classroomId
        ))
    }

    fun getLecturersWithNameContainingSync(namePart: String) =
            lecturerRepository.getLecturersWithNameContainingSync(namePart)

    fun getBuildingsFilteredByNameSync(query: String) =
            buildingRepository.getBuildingsFilteredByName(query)

    fun getClassroomsWithNameOrCodeContainingSync(query: String) =
            classroomRepository.getClassroomsWithNameOrCodeContainingSync(query)

    fun validateFormData(
            building: Building?,
            lecturer: Lecturer?,
            classroom: Classroom?,
            courseCode: String,
            courseName: String
    ) {
        if (courseName.isEmpty()) throw EmptyCourseNameException()
        if (courseCode.isEmpty()) throw EmptyCourseCodeException()
        if (building == null) throw NoSuchBuildingException()
        if (classroom == null) throw NoSuchClassroomException()
        if (lecturer == null) throw NoSuchLecturerException()
    }

    fun isClassroomInBuilding(classroom: Classroom, building: Building): LiveData<Boolean> =
            Transformations.map(
                    classroomRepository
                            .countClassroomsOfBuildingWithCodeLike(
                                    buildingId = building.id,
                                    classroomId = classroom.id)
            ) { numberOfBuildingsMatched ->
                numberOfBuildingsMatched == 1
            }
}