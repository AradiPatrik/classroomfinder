package hu.aradipatrik.classroomfinder.ui.dashboard

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.AsyncDifferConfig
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import hu.aradipatrik.classroomfinder.AppExecutors
import hu.aradipatrik.classroomfinder.R
import hu.aradipatrik.classroomfinder.db.pokos.CourseCardItem
import hu.aradipatrik.classroomfinder.databinding.LectureCardBinding
import hu.aradipatrik.classroomfinder.service.ImageLoaderService

class CourseAdapter(
    appExecutors: AppExecutors,
    private val imageLoaderService: ImageLoaderService,
    private val onToCourseClick: (CourseCardItem) -> Unit,
    private val onToLecturerClick: (CourseCardItem) -> Unit
) : ListAdapter<CourseCardItem, CourseAdapter.ViewHolder>(
        AsyncDifferConfig.Builder(CourseDiffCallback())
                .setBackgroundThreadExecutor(appExecutors.diskIO())
                .build()
) {

    override fun onCreateViewHolder(parent: ViewGroup, position: Int) =
            ViewHolder(
                    DataBindingUtil.inflate(
                            LayoutInflater.from(parent.context),
                            R.layout.lecture_card, parent, false
                    ), imageLoaderService, onToCourseClick, onToLecturerClick
            )

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val lecture = getItem(position)
        holder.itemView.tag = lecture
        holder.bind(lecture)
    }

    class ViewHolder(
        private val binding: LectureCardBinding,
        private val imageLoaderService: ImageLoaderService,
        private val onToCourseClick: (CourseCardItem) -> Unit,
        private val onToLecturerClick: (CourseCardItem) -> Unit
    ) :
            RecyclerView.ViewHolder(binding.root) {
        fun bind(courseCardItem: CourseCardItem) {
            binding.apply {
                imageLoaderService.loadImageInto(
                        imageUrl = courseCardItem.buildingImage, imageView = buildingPicture)
                lectureName.text = courseCardItem.courseName
                lecturerName.text = courseCardItem.lecturerName
                buttonToCourse.setOnClickListener { onToCourseClick(courseCardItem) }
                buttonToLecturer.setOnClickListener { onToLecturerClick(courseCardItem) }
                executePendingBindings()
            }
        }
    }
}