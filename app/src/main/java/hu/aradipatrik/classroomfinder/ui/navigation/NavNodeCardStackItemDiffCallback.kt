package hu.aradipatrik.classroomfinder.ui.navigation

import androidx.recyclerview.widget.DiffUtil
import hu.aradipatrik.classroomfinder.db.pokos.NavNodeCardStackItem

class NavNodeCardStackItemDiffCallback : DiffUtil.ItemCallback<NavNodeCardStackItem>() {
    override fun areItemsTheSame(
        oldItem: NavNodeCardStackItem,
        newItem: NavNodeCardStackItem
    ) =
            oldItem.id == newItem.id

    override fun areContentsTheSame(
        oldItem: NavNodeCardStackItem,
        newItem: NavNodeCardStackItem
    ) =
            oldItem == newItem
}