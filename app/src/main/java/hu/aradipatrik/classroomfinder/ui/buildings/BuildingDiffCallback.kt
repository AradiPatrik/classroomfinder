package hu.aradipatrik.classroomfinder.ui.buildings

import androidx.recyclerview.widget.DiffUtil
import hu.aradipatrik.classroomfinder.db.entities.Building

class BuildingDiffCallback : DiffUtil.ItemCallback<Building>() {
    override fun areItemsTheSame(oldItem: Building, newItem: Building) =
            oldItem.name == newItem.name

    override fun areContentsTheSame(oldItem: Building, newItem: Building) =
            oldItem == newItem
}