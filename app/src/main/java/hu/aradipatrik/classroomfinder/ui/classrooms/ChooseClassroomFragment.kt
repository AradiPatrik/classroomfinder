package hu.aradipatrik.classroomfinder.ui.classrooms

import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.NavHostFragment
import hu.aradipatrik.classroomfinder.AppExecutors
import hu.aradipatrik.classroomfinder.FabManager
import hu.aradipatrik.classroomfinder.R
import hu.aradipatrik.classroomfinder.SearchViewOwner
import hu.aradipatrik.classroomfinder.db.pokos.ClassroomEssentials
import hu.aradipatrik.classroomfinder.databinding.FragmentChooseClassroomBinding
import hu.aradipatrik.classroomfinder.injection.Injectable
import hu.aradipatrik.classroomfinder.service.GoogleMapsService
import hu.aradipatrik.classroomfinder.testing.OpenForTesting
import javax.inject.Inject

/**
 * The UI Controller for displaying a the classrooms in the previously selected building
 */
@OpenForTesting
class ChooseClassroomFragment : Fragment(), Injectable {
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var fabManager: FabManager

    @Inject
    lateinit var searchViewOwner: SearchViewOwner

    @Inject
    lateinit var googleMapsService: GoogleMapsService

    @Inject
    lateinit var appExecutors: AppExecutors

    private lateinit var viewModel: ChooseClassroomViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding =
                FragmentChooseClassroomBinding.inflate(inflater, container, false)
        val args = ChooseClassroomFragmentArgs.fromBundle(arguments!!)
        viewModel = ViewModelProviders
                .of(activity!!, viewModelFactory)
                .get(ChooseClassroomViewModel::class.java)

        viewModel.setBuilding(args.buildingName, args.buildingId)

        fabManager.setFabVisible(false)

        val adapter = ClassroomAdapter(appExecutors, this::onClassroomClick)

        binding.chooseClassroomRecyclerView.adapter = adapter
        subscribeUi(adapter)

        initSearch()

        setHasOptionsMenu(true)

        return binding.root
    }

    private fun initSearch() {
        searchViewOwner.getSearchView().onQueryChange { viewModel.queryClassrooms(it) }
        searchViewOwner.getSearchView().onShow { viewModel.queryClassrooms("") }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_search, menu)
        searchViewOwner.getSearchView().setMenuItem(menu.findItem(R.id.action_search))
        super.onCreateOptionsMenu(menu, inflater)
    }

    private fun onClassroomClick(classroom: ClassroomEssentials) {
        searchViewOwner.getSearchView().closeSearch()
        googleMapsService.navigateTo(viewModel.getBuildingName())
        navController().navigate(
                ChooseClassroomFragmentDirections.navigateToClassroom(classroom.id))
    }

    private fun subscribeUi(adapter: ClassroomAdapter) {
        viewModel.classrooms.observe(viewLifecycleOwner, Observer { classrooms ->
            if (classrooms != null) adapter.submitList(classrooms)
        })
    }

    override fun onDetach() {
        super.onDetach()
        searchViewOwner.getSearchView().closeSearch()
    }

    fun navController() = NavHostFragment.findNavController(this)
}
