package hu.aradipatrik.classroomfinder.ui.createcourse

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.NavHostFragment
import com.github.kittinunf.result.Result
import com.github.kittinunf.result.failure
import com.github.kittinunf.result.map
import com.google.android.material.snackbar.Snackbar
import hu.aradipatrik.classroomfinder.AppBorderManager
import hu.aradipatrik.classroomfinder.AppExecutors
import hu.aradipatrik.classroomfinder.FabManager
import hu.aradipatrik.classroomfinder.R
import hu.aradipatrik.classroomfinder.databinding.FragmentCreateCourseBinding
import hu.aradipatrik.classroomfinder.db.AUTO_GENERATE_ID
import hu.aradipatrik.classroomfinder.db.entities.Building
import hu.aradipatrik.classroomfinder.db.entities.Classroom
import hu.aradipatrik.classroomfinder.db.entities.Course
import hu.aradipatrik.classroomfinder.db.entities.Lecturer
import hu.aradipatrik.classroomfinder.injection.ACTIVITY_CONTEXT
import hu.aradipatrik.classroomfinder.injection.Injectable
import hu.aradipatrik.classroomfinder.service.DialogFactory
import hu.aradipatrik.classroomfinder.testing.OpenForTesting
import javax.inject.Inject
import javax.inject.Named

const val EXCEPTION_WITHOUT_ERROR_MESSAGE = "Form Error indicated without message resource"

/**
 * The UI Controller for adding user defined courses to their dashboard
 */
@OpenForTesting
class CreateCourseFragment : Fragment(), Injectable {
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var appBorderManager: AppBorderManager

    @Inject
    lateinit var fabManager: FabManager

    @Inject
    lateinit var dialogService: DialogFactory

    @Inject
    @field:Named(ACTIVITY_CONTEXT)
    lateinit var activityContext: Context

    @Inject
    lateinit var appExecutors: AppExecutors

    private lateinit var viewModel: CreateCourseViewModel
    private lateinit var binding: FragmentCreateCourseBinding

    private var selectedBuilding: Building? = null
    private var selectedClassroom: Classroom? = null
    private var selectedLecturer: Lecturer? = null

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        binding = FragmentCreateCourseBinding.inflate(inflater, container, false)

        val classroomAutocompleteAdapter = AutocompleteListAdapter(
                activityContext,
                resourceId = R.layout.classroom_autocomplete_item,
                viewBinder = this::bindClassroomToClassroomView,
                valueFilter = this::filterClassroom,
                toStringMapper = { it.name ?: "" })

        val buildingAutocompleteListAdapter = AutocompleteListAdapter(
                activityContext,
                resourceId = R.layout.building_autocomplete_item,
                viewBinder = this::bindBuildingToBuildingView,
                valueFilter = this::filterBuildings,
                toStringMapper = { it.name })

        val lecturerAutocompleteListAdapter = AutocompleteListAdapter(
                activityContext,
                resourceId = R.layout.lecturer_autocomplete_item,
                viewBinder = this::bindLecturerToLecturerView,
                valueFilter = this::filterLecturers,
                toStringMapper = { it.name }
        )

        viewModel = ViewModelProviders
                .of(this, viewModelFactory)
                .get(CreateCourseViewModel::class.java)

        binding.classroomNameAutoComplete.setAdapter(classroomAutocompleteAdapter)
        binding.buildingNameAutoComplete.setAdapter(buildingAutocompleteListAdapter)
        binding.lecturerNameAutoComplete.setAdapter(lecturerAutocompleteListAdapter)

        binding.classroomNameAutoComplete.onItemClickListener =
                AdapterView.OnItemClickListener { _, _, i, _ ->
                    selectedClassroom = classroomAutocompleteAdapter.getItem(i)
                }

        binding.buildingNameAutoComplete.onItemClickListener =
                AdapterView.OnItemClickListener { _, _, i, _ ->
                    selectedBuilding = buildingAutocompleteListAdapter.getItem(i)
                }

        binding.lecturerNameAutoComplete.onItemClickListener =
                AdapterView.OnItemClickListener { _, _, i, _ ->
                    selectedLecturer = lecturerAutocompleteListAdapter.getItem(i)
                }

        if (deviceIsNotRotated(savedInstanceState)) {
            appBorderManager.showNavAndToolBar()
        }

        fabManager.setFabVisible(true)
        fabManager.setFabIcon(R.drawable.ic_add_white)
        fabManager.setFabClickListener {
            validateAndCreateCourse()
        }

        return binding.root
    }

    private fun deviceIsNotRotated(savedInstanceState: Bundle?) = savedInstanceState == null

    private fun validateAndCreateCourse() {
        Result.of<Unit, Exception> { validateForm() }
                .map { createCourseIfClassroomInBuilding() }
                .failure { handleFormError(it) }
    }

    private fun validateForm() = viewModel.validateFormData(
            selectedBuilding, selectedLecturer, selectedClassroom,
            binding.courseCodeEditText.text.toString(),
            binding.courseNameEditText.text.toString())

    private fun createCourseIfClassroomInBuilding() =
            viewModel.isClassroomInBuilding(selectedClassroom!!, selectedBuilding!!)
                    .observe(viewLifecycleOwner, Observer { isClassroomInBuilding ->
                        if (isClassroomInBuilding) addCourseToDb()
                        else notifyUserClassroomNotInBuilding()
                    })

    private fun handleFormError(ex: Exception): Any = when (ex) {
        is ExceptionWithMessage -> when (ex) {
            is EmptyCourseNameException -> notifyUserEmptyCourseName(ex)
            is EmptyCourseCodeException -> notifyUserEmptyCourseCode(ex)
            is NoSuchBuildingException -> notifyUserNoSuchBuilding(ex)
            is NoSuchClassroomException -> notifyUserNoSuchClassroom(ex)
            is NoSuchLecturerException -> notifyUserNoSuchLecturer(ex)
        }
        else -> {
            Exception(EXCEPTION_WITHOUT_ERROR_MESSAGE)
        }
    }

    private fun notifyUserEmptyCourseName(e: EmptyCourseNameException) {
        binding.courseNameEditText.error = resources.getString(e.messageResourceId)
    }

    private fun notifyUserEmptyCourseCode(e: EmptyCourseCodeException) {
        binding.courseCodeEditText.error = resources.getString(e.messageResourceId)
    }

    private fun notifyUserNoSuchClassroom(e: NoSuchClassroomException) {
        binding.classroomNameAutoComplete.error = resources.getString(e.messageResourceId)
    }

    private fun notifyUserNoSuchLecturer(e: NoSuchLecturerException) {
        showNoSuchLecturerDialog()
        binding.lecturerNameAutoComplete.error = resources.getString(e.messageResourceId)
    }

    private fun notifyUserNoSuchBuilding(e: NoSuchBuildingException) {
        binding.buildingNameAutoComplete.error = resources.getString(e.messageResourceId)
    }

    private fun showNoSuchLecturerDialog() = dialogService.createDialog(
            titleResourceId = R.string.no_such_lecturer_dialog_title,
            bodyResourceId = R.string.no_such_lecturer_dialog_body,
            negativeButtonResourceId = R.string.no_such_lecturer_negative_button,
            positiveButtonResourceId = R.string.no_such_lecturer_positive_button,
            onPositiveClick = {
                viewModel.createCourseWithUnknownLecturer(
                        binding.courseNameEditText.text.toString(),
                        binding.courseCodeEditText.text.toString(),
                        classroomId = selectedClassroom!!.id,
                        lecturerName = binding.lecturerNameAutoComplete.text.toString()
                )
                backToDashboard()
            }
    ).showDialog()

    private fun addCourseToDb() {
        insertCourse()
        showInsertionSuccessMessage()
    }

    private fun insertCourse() {
        viewModel.createCourse(
                Course(
                        id = AUTO_GENERATE_ID,
                        code = binding.courseCodeEditText.text.toString(),
                        name = binding.courseNameEditText.text.toString(),
                        lecturerId = selectedLecturer!!.id,
                        classroomId = selectedClassroom!!.id
                )
        )
    }

    private fun showInsertionSuccessMessage() {
        Snackbar.make(
                binding.root,
                resources.getString(R.string.classroom_create_successful),
                Snackbar.LENGTH_INDEFINITE
        ).setAction("OK") { backToDashboard() }.show()
    }

    private fun notifyUserClassroomNotInBuilding() {
        binding.classroomNameAutoComplete.error =
                resources.getString(R.string.classroom_not_in_building)
    }

    private fun bindBuildingToBuildingView(building: Building, view: View) {
        view.findViewById<TextView>(R.id.building_name).text = building.name
    }

    private fun filterBuildings(query: CharSequence): List<Building> {
        val matched = viewModel.getBuildingsFilteredByNameSync(query.toString())
        selectedBuilding = if (matched.size == 1) {
            matched[0]
        } else {
            null
        }
        return matched
    }

    private fun bindClassroomToClassroomView(classroom: Classroom, view: View) {
        view.findViewById<TextView>(R.id.classroom_name).text = classroom.name
    }

    private fun bindLecturerToLecturerView(lecturer: Lecturer, view: View) {
        view.findViewById<TextView>(R.id.lecturer_name).text = lecturer.name
    }

    private fun <T> isExactMatch(matched: List<T>) =
            matched.size == 1

    private fun <T> getOnlyItemOrNull(matched: List<T>) =
            if (isExactMatch(matched)) matched.first() else null

    private fun filterClassroom(query: CharSequence): List<Classroom> {
        val matched = viewModel
                .getClassroomsWithNameOrCodeContainingSync(query.toString())
        selectedClassroom = getOnlyItemOrNull(matched)
        return matched
    }

    private fun filterLecturers(query: CharSequence): List<Lecturer> {
        val matched = viewModel
                .getLecturersWithNameContainingSync(query.toString())
        selectedLecturer = getOnlyItemOrNull(matched)
        return matched
    }

    private fun backToDashboard() {
        navController().navigate(R.id.dashboardFragment)
    }

    open fun navController() = NavHostFragment.findNavController(this)
}
