package hu.aradipatrik.classroomfinder.ui.classrooms

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.AsyncDifferConfig
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import hu.aradipatrik.classroomfinder.AppExecutors
import hu.aradipatrik.classroomfinder.R
import hu.aradipatrik.classroomfinder.db.pokos.ClassroomEssentials
import hu.aradipatrik.classroomfinder.databinding.ClassroomCardBinding

class ClassroomAdapter(
        appExecutors: AppExecutors,
        private val onClassroomClick: (classroom: ClassroomEssentials) -> Unit
) : ListAdapter<ClassroomEssentials, ClassroomAdapter.ViewHolder>(
        AsyncDifferConfig.Builder(ClassroomDiffCallback())
                .setBackgroundThreadExecutor(appExecutors.diskIO())
                .build()
) {
    override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {
        return ViewHolder(
                DataBindingUtil.inflate(
                        LayoutInflater.from(parent.context),
                        R.layout.classroom_card, parent, false
                ), onClassroomClick
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val classroom = getItem(position)
        holder.itemView.tag = classroom
        holder.bind(classroom)
    }

    class ViewHolder(
            private val binding: ClassroomCardBinding,
            private val onClassroomClick: (classroom: ClassroomEssentials) -> Unit
    ) :
            RecyclerView.ViewHolder(binding.root) {
        fun bind(classroom: ClassroomEssentials) {
            binding.apply {
                binding.classroomCode.text = classroom.code
                binding.classroomName.text = classroom.name
                binding.cardView.setOnClickListener {
                    onClassroomClick(classroom)
                }
                executePendingBindings()
            }
        }
    }
}