package hu.aradipatrik.classroomfinder.ui.buildings

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.NavHostFragment
import hu.aradipatrik.classroomfinder.AppExecutors
import hu.aradipatrik.classroomfinder.FabManager
import hu.aradipatrik.classroomfinder.db.entities.Building
import hu.aradipatrik.classroomfinder.databinding.FragmentChooseBuildingBinding
import hu.aradipatrik.classroomfinder.injection.Injectable
import hu.aradipatrik.classroomfinder.service.GoogleMapsService
import hu.aradipatrik.classroomfinder.service.ImageLoaderService
import hu.aradipatrik.classroomfinder.testing.OpenForTesting
import javax.inject.Inject

/**
 * The UI Controller for displaying a the available buildings and their actions
 * which contains navigating to the building and listing it's classrooms.
 */
@OpenForTesting
class ChooseBuildingFragment : Fragment(), Injectable {
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var fabManager: FabManager

    @Inject
    lateinit var googleMapsService: GoogleMapsService

    @Inject
    lateinit var imageLoaderService: ImageLoaderService

    @Inject
    lateinit var appExecutors: AppExecutors

    private lateinit var viewModel: ChooseBuildingViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding =
                FragmentChooseBuildingBinding.inflate(inflater, container, false)
        viewModel = ViewModelProviders
                .of(activity!!, viewModelFactory)
                .get(ChooseBuildingViewModel::class.java)

        fabManager.setFabVisible(false)

        val adapter = BuildingAdapter(
                appExecutors,
                imageLoaderService,
                this::onListClassroomsButtonClick,
                this::onNavigateToBuildingClick)
        binding.chooseBuildingRecyclerView.adapter = adapter
        subscribeUi(adapter)

        return binding.root
    }

    private fun onListClassroomsButtonClick(building: Building) {
        navController().navigate(
                ChooseBuildingFragmentDirections.chooseBuilding(building.id, building.name))
    }

    private fun onNavigateToBuildingClick(building: Building) {
        googleMapsService.navigateTo(building.name)
    }

    private fun subscribeUi(adapter: BuildingAdapter) {
        viewModel.buildings.observe(viewLifecycleOwner, Observer { buildings ->
            if (buildings != null) adapter.submitList(buildings)
        })
    }

    fun navController() = NavHostFragment.findNavController(this)
}
