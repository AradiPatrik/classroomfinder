package hu.aradipatrik.classroomfinder.view.custom

import android.view.MenuItem
import com.ferfalk.simplesearchview.SimpleSearchView

interface SearchViewWrapper {
    fun onQueryChange(queryChangeListener: (String) -> Unit)
    fun onShow(onShowAction: () -> Unit)
    fun setMenuItem(menuItem: MenuItem)
    fun closeSearch()
}

class SearchViewWrapperImpl(private val searchView: SimpleSearchView)
    : SearchViewWrapper {
    override fun onQueryChange(queryChangeListener: (String) -> Unit) =
            searchView.setOnQueryTextListener(object : SimpleSearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(query: String?): Boolean {
                    queryChangeListener(query ?: "")
                    return true
                }

                override fun onQueryTextChange(newText: String?): Boolean {
                    queryChangeListener(newText ?: "")
                    return false
                }

                override fun onQueryTextCleared(): Boolean {
                    return false
                }
            })

    override fun onShow(onShowAction: () -> Unit) =
            searchView.setOnSearchViewListener(object : SimpleSearchView.SearchViewListener {
                override fun onSearchViewShownAnimation() {}

                override fun onSearchViewClosed() {}

                override fun onSearchViewClosedAnimation() {}

                override fun onSearchViewShown() {
                    onShowAction()
                }
            })

    override fun setMenuItem(menuItem: MenuItem) = searchView.setMenuItem(menuItem)

    override fun closeSearch() = searchView.closeSearch()
}
