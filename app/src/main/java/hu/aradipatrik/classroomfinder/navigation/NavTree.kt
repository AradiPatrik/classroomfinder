package hu.aradipatrik.classroomfinder.navigation

import hu.aradipatrik.classroomfinder.db.entities.NavNode
import java.util.ArrayDeque
import javax.inject.Inject
import kotlin.collections.ArrayList

interface NavTree {
    fun getDepthFirstOrdering(): List<NavNode>
    fun getRouteToNode(id: Int): List<NavNode>
}

class NavTreeFactory @Inject constructor() {
    fun createFromNavNodeList(navNodes: List<NavNode>) =
            ArrayNavTree(navNodes)
}

class ArrayNavTree(private val navNodes: List<NavNode>) : NavTree {
    private val root: NavNode = navNodes.first { it.parentId == null }

    override fun getDepthFirstOrdering() =
            NavTreeOrganiser(navNodes, root).getDepthFirstOrdering()

    override fun getRouteToNode(id: Int) =
            NavTreeSearcher(navNodes, id).getPath()
}

private class NavTreeOrganiser(
    val navNodes: List<NavNode>,
    root: NavNode
) {
    val orderedNodes = ArrayList<NavNode>(navNodes.size)
    val openSet = ArrayDeque<NavNode>()
    lateinit var currentNode: NavNode

    init {
        openSet.push(root)
    }

    fun getDepthFirstOrdering(): List<NavNode> {
        while (openSet.isNotEmpty())
            explicateNextNode()

        return orderedNodes
    }

    fun explicateNextNode() {
        currentNode = openSet.pop()
        orderedNodes.add(currentNode)
        addChildrenOfCurrentNodeToOpenSet()
    }

    fun addChildrenOfCurrentNodeToOpenSet() =
            navNodes
                    .filter { currentNode.id == it.parentId }
                    .forEach { openSet.push(it) }
}

private class NavTreeSearcher(
    private val navNodes: List<NavNode>,
    private val soughtId: Int
) {
    val routeAsNavNodes = ArrayList<NavNode>()
    val soughtNode = navNodes.first { it.id == soughtId }
    var currentNode = soughtNode

    fun getPath(): List<NavNode> {
        routeAsNavNodes.add(currentNode)
        walkUpThroughParentNodes()
        routeAsNavNodes.reverse()
        return routeAsNavNodes
    }

    private fun walkUpThroughParentNodes() {
        while (currentNode.parentId != null)
            addNextNodeToRoute()
    }

    private fun addNextNodeToRoute() {
        currentNode = navNodes.first { it.id == currentNode.parentId }
        routeAsNavNodes.add(currentNode)
    }
}