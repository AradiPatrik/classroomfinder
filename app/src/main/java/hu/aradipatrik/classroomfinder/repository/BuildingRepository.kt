package hu.aradipatrik.classroomfinder.repository

import androidx.lifecycle.LiveData
import hu.aradipatrik.classroomfinder.db.daos.BuildingDao
import hu.aradipatrik.classroomfinder.db.entities.Building
import javax.inject.Inject

class BuildingRepository @Inject constructor(private val buildingDao: BuildingDao) {
    fun getBuildingsFilteredByName(buildingName: String) =
            buildingDao.getBuildingsFilteredByNameSync(buildingName)

    fun getBuildingById(id: Int): LiveData<Building> =
            buildingDao.getBuildingById(id)

    val allBuildings: LiveData<List<Building>> = buildingDao.getAllBuildings()
}