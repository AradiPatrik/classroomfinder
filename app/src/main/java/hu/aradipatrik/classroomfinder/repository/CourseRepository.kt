package hu.aradipatrik.classroomfinder.repository

import androidx.lifecycle.LiveData
import hu.aradipatrik.classroomfinder.db.daos.CourseDao
import hu.aradipatrik.classroomfinder.db.entities.Course
import hu.aradipatrik.classroomfinder.db.pokos.CourseLocation
import hu.aradipatrik.classroomfinder.db.pokos.CourseCardItem
import javax.inject.Inject

class CourseRepository @Inject constructor(private val courseDao: CourseDao) {
    val allCourses: LiveData<List<CourseCardItem>> = courseDao.getAllCourses()

    fun getCourseLocation(courseId: Int): LiveData<CourseLocation> =
            courseDao.getCourseLocation(courseId)

    fun insertCourse(course: Course) = courseDao.insertAll(listOf(course))
}
