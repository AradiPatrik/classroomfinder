package hu.aradipatrik.classroomfinder.repository

import hu.aradipatrik.classroomfinder.db.daos.NavNodeDao
import javax.inject.Inject

class NavNodeRepository @Inject constructor(private val navNodeDao: NavNodeDao) {
    fun getNavNodesOfBuilding(buildingId: Int) =
            navNodeDao.getNavNodesInBuilding(buildingId)
}