package hu.aradipatrik.classroomfinder.repository

import hu.aradipatrik.classroomfinder.db.daos.ClassroomDao
import javax.inject.Inject

class ClassroomRepository @Inject constructor(private val classroomDao: ClassroomDao) {
    fun getAllClassroomsOfBuildingFiltered(id: Int, query: String) =
                classroomDao.getClassroomsOfBuildingWithNameOrCodeLike(id, query)

    fun getClassroomsWithNameOrCodeContainingSync(query: String) =
            classroomDao.getClassroomsWithNameOrCodeContainingSync(query)

    fun getClassroomById(classroomId: Int) = classroomDao.getClassroomById(classroomId)

    fun countClassroomsOfBuildingWithCodeLike(buildingId: Int, classroomId: Int) =
            classroomDao.isClassroomInBuilding(buildingId, classroomId)
}
