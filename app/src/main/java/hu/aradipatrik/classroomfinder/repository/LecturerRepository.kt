package hu.aradipatrik.classroomfinder.repository

import hu.aradipatrik.classroomfinder.db.AUTO_GENERATE_ID
import hu.aradipatrik.classroomfinder.db.NON_EXISTENT_CLASSROOM_ID
import hu.aradipatrik.classroomfinder.db.daos.LecturerDao
import hu.aradipatrik.classroomfinder.db.entities.Lecturer
import javax.inject.Inject

class LecturerRepository @Inject constructor(private val lecturerDao: LecturerDao) {
    fun getOfficeOfLecturer(id: Int) = lecturerDao.getOfficeOfLecturer(id)

    fun getLecturersWithNameContainingSync(namePart: String) =
            lecturerDao.getLecturersWithNameContainingSync(namePart)

    fun insertLecturerWithoutOffice(name: String) = lecturerDao.insertAll(listOf(
            Lecturer(AUTO_GENERATE_ID, "", "", name, null, NON_EXISTENT_CLASSROOM_ID)
    ))
}