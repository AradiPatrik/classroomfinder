package hu.aradipatrik.classroomfinder.injection

import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton
import android.app.Application
import dagger.BindsInstance
import hu.aradipatrik.classroomfinder.ClassroomFinderApplication

@Suppress("unused")
@Singleton
@Component(modules = [
    AndroidInjectionModule::class,
    MainActivityModule::class,
    DatabaseModule::class
])
interface AppComponent {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }

    fun inject(classroomFinderApplication: ClassroomFinderApplication)
}