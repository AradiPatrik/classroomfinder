package hu.aradipatrik.classroomfinder.injection

import android.app.Application
import android.content.Context
import dagger.Binds
import dagger.Module
import hu.aradipatrik.classroomfinder.AppBorderManager
import hu.aradipatrik.classroomfinder.FabManager
import hu.aradipatrik.classroomfinder.MainActivity
import hu.aradipatrik.classroomfinder.SearchViewOwner
import hu.aradipatrik.classroomfinder.service.DialogFactory
import hu.aradipatrik.classroomfinder.service.MaterialDialogFactory
import javax.inject.Named

const val APP_CONTEXT = "applicationContext"
const val ACTIVITY_CONTEXT = "activityContext"

@Suppress("unused")
@Module(includes = [ViewModelModule::class, ServiceModule::class])
abstract class AppModule {
    @Binds
    abstract fun bindFabManager(mainActivity: MainActivity): FabManager

    @Binds
    abstract fun bindAppBorderManager(mainActivity: MainActivity): AppBorderManager

    @Binds
    abstract fun bindSearchViewOwner(mainActivity: MainActivity): SearchViewOwner

    @Binds
    abstract fun bindDialogService(dialogService: MaterialDialogFactory): DialogFactory

    @Binds
    @Named(ACTIVITY_CONTEXT)
    abstract fun bindActivityContext(mainActivity: MainActivity): Context

    @Binds
    @Named(APP_CONTEXT)
    abstract fun bindApplicationContext(application: Application): Context
}