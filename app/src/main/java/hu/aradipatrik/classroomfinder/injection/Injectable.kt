package hu.aradipatrik.classroomfinder.injection

/**
 * Marks an activity / fragment injectable.
 */
interface Injectable