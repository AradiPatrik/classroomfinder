package hu.aradipatrik.classroomfinder.injection

import dagger.Module
import dagger.android.ContributesAndroidInjector
import hu.aradipatrik.classroomfinder.MainActivity

@Suppress("unused")
@Module
abstract class MainActivityModule {
    @ContributesAndroidInjector(modules = [
        FragmentBuildersModule::class,
        AppModule::class])
    abstract fun contributeMainActivity(): MainActivity
}