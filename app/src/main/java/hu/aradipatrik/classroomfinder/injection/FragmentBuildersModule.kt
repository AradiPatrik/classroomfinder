package hu.aradipatrik.classroomfinder.injection

import dagger.Module
import dagger.android.ContributesAndroidInjector
import hu.aradipatrik.classroomfinder.ui.buildings.ChooseBuildingFragment
import hu.aradipatrik.classroomfinder.ui.classrooms.ChooseClassroomFragment
import hu.aradipatrik.classroomfinder.ui.createcourse.CreateCourseFragment
import hu.aradipatrik.classroomfinder.ui.dashboard.DashboardFragment
import hu.aradipatrik.classroomfinder.ui.navigation.InBuildingNavigationFragment

@Suppress("unused")
@Module
abstract class FragmentBuildersModule {
    @ContributesAndroidInjector
    abstract fun contributeChooseBuildingFragment(): ChooseBuildingFragment

    @ContributesAndroidInjector
    abstract fun contributeChooseClassroomFragment(): ChooseClassroomFragment

    @ContributesAndroidInjector
    abstract fun contributeCreateCourseFragment(): CreateCourseFragment

    @ContributesAndroidInjector
    abstract fun contributeDashboardFragment(): DashboardFragment

    @ContributesAndroidInjector
    abstract fun contributeInBuildingNavigationFragment(): InBuildingNavigationFragment
}