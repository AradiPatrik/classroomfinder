package hu.aradipatrik.classroomfinder.injection

import dagger.Binds
import dagger.Module
import hu.aradipatrik.classroomfinder.service.ActionTranslatorService
import hu.aradipatrik.classroomfinder.service.ActionTranslatorServiceImpl
import hu.aradipatrik.classroomfinder.service.ActivityStarterService
import hu.aradipatrik.classroomfinder.service.ActivityStarterServiceImpl
import hu.aradipatrik.classroomfinder.service.CalendarParserService
import hu.aradipatrik.classroomfinder.service.CalendarParserServiceImpl
import hu.aradipatrik.classroomfinder.service.GoogleMapsService
import hu.aradipatrik.classroomfinder.service.GoogleMapsServiceImpl
import hu.aradipatrik.classroomfinder.service.HttpService
import hu.aradipatrik.classroomfinder.service.ImageLoaderService
import hu.aradipatrik.classroomfinder.service.NotificationService
import hu.aradipatrik.classroomfinder.service.NotificationServiceImpl
import hu.aradipatrik.classroomfinder.service.PicassoImageLoaderService
import hu.aradipatrik.classroomfinder.service.ReflectionResourceIdService
import hu.aradipatrik.classroomfinder.service.ResourceIdService
import hu.aradipatrik.classroomfinder.service.ResourceService
import hu.aradipatrik.classroomfinder.service.ResourceServiceImpl
import hu.aradipatrik.classroomfinder.service.VolleyHttpService

@Suppress("unused")
@Module
abstract class ServiceModule {
    @Binds
    abstract fun bindActionTranslatorService(
            actionTranslatorServiceImpl: ActionTranslatorServiceImpl
    ): ActionTranslatorService

    @Binds
    abstract fun bindCalendarParser(
            calendarParserImpl: CalendarParserServiceImpl
    ): CalendarParserService

    @Binds
    abstract fun bindHttpService(
            volleyHttpService: VolleyHttpService
    ): HttpService

    @Binds
    abstract fun bindGoogleMapsService(
            googleMapsService: GoogleMapsServiceImpl
    ): GoogleMapsService

    @Binds
    abstract fun bindResourceIdService(
            resourceIdService: ReflectionResourceIdService
    ): ResourceIdService

    @Binds
    abstract fun bindNotificationService(
            notificationService: NotificationServiceImpl
    ): NotificationService

    @Binds
    abstract fun bindResourceService(
            resourceServiceImpl: ResourceServiceImpl
    ): ResourceService

    @Binds
    abstract fun bindActivityStarterService(
            activityStarterServiceImpl: ActivityStarterServiceImpl
    ): ActivityStarterService

    @Binds
    abstract fun bindImageLoaderService(
            picassoImageLoaderService: PicassoImageLoaderService
    ): ImageLoaderService
}