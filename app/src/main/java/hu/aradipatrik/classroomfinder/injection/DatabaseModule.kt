package hu.aradipatrik.classroomfinder.injection

import android.app.Application
import android.util.Log
import androidx.room.Room
import dagger.Module
import dagger.Provides
import hu.aradipatrik.classroomfinder.AppExecutors
import hu.aradipatrik.classroomfinder.db.AppDatabase
import hu.aradipatrik.classroomfinder.db.DATABASE_NAME
import hu.aradipatrik.classroomfinder.db.DatabaseSeeder
import hu.aradipatrik.classroomfinder.db.daos.BuildingDao
import hu.aradipatrik.classroomfinder.db.daos.ClassroomDao
import hu.aradipatrik.classroomfinder.db.daos.CourseDao
import hu.aradipatrik.classroomfinder.db.daos.LecturerDao
import hu.aradipatrik.classroomfinder.db.daos.NavNodeDao
import hu.aradipatrik.classroomfinder.navigation.NavTreeFactory
import hu.aradipatrik.classroomfinder.util.onCreated
import javax.inject.Singleton

@Module
class DatabaseModule {
    private lateinit var appDatabase: AppDatabase

    @Singleton
    @Provides
    fun provideAppDb(
        application: Application,
        navTreeFactory: NavTreeFactory,
        appExecutors: AppExecutors
    ): AppDatabase {
        appDatabase = Room.databaseBuilder(application, AppDatabase::class.java, DATABASE_NAME)
                .fallbackToDestructiveMigration()
                .onCreated {
                    DatabaseSeeder(
                            buildingDao = appDatabase.buildingDao(),
                            classroomDao = appDatabase.classroomDao(),
                            courseDao = appDatabase.courseDao(),
                            lecturerDao = appDatabase.lecturerDao(),
                            navNodeDao = appDatabase.navNodeDao(),
                            navTreeFactory = navTreeFactory,
                            appExecutors = appExecutors
                    ).seedDatabase()
                }.build()
        return appDatabase
    }

    @Singleton
    @Provides
    fun provideBuildingDao(appDatabase: AppDatabase): BuildingDao {
        return appDatabase.buildingDao()
    }

    @Singleton
    @Provides
    fun provideClassroomDao(appDatabase: AppDatabase): ClassroomDao {
        return appDatabase.classroomDao()
    }

    @Singleton
    @Provides
    fun provideCourseDao(appDatabase: AppDatabase): CourseDao {
        return appDatabase.courseDao()
    }

    @Singleton
    @Provides
    fun provideLecturerDao(appDatabase: AppDatabase): LecturerDao {
        return appDatabase.lecturerDao()
    }

    @Singleton
    @Provides
    fun provideNavNodeDao(appDatabase: AppDatabase): NavNodeDao {
        return appDatabase.navNodeDao()
    }
}