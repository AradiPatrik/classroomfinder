package hu.aradipatrik.classroomfinder.injection

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import hu.aradipatrik.classroomfinder.ui.buildings.ChooseBuildingViewModel
import hu.aradipatrik.classroomfinder.ui.classrooms.ChooseClassroomViewModel
import hu.aradipatrik.classroomfinder.ui.createcourse.CreateCourseViewModel
import hu.aradipatrik.classroomfinder.ui.dashboard.DashboardViewModel
import hu.aradipatrik.classroomfinder.ui.navigation.NavigationViewModel
import hu.aradipatrik.classroomfinder.viewmodel.ClassroomFinderViewModelFactory

@Suppress("unused")
@Module
abstract class ViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(CreateCourseViewModel::class)
    abstract fun bindCreateCourseViewModel(createCourseViewModel: CreateCourseViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(DashboardViewModel::class)
    abstract fun bindDashboardViewModel(dashboardViewModel: DashboardViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(NavigationViewModel::class)
    abstract fun bindNavigationViewModel(navigationViewModel: NavigationViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ChooseBuildingViewModel::class)
    abstract fun bindChooseBuildingViewModel(chooseBuildingViewModel: ChooseBuildingViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ChooseClassroomViewModel::class)
    abstract fun bindChooseClassroomViewModel(chooseClassroomViewModel: ChooseClassroomViewModel): ViewModel

    @Binds
    abstract fun bindViewModelFactory(factory: ClassroomFinderViewModelFactory):
            ViewModelProvider.Factory
}