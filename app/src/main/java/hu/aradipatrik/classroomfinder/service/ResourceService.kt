package hu.aradipatrik.classroomfinder.service

import android.content.Context
import hu.aradipatrik.classroomfinder.R
import hu.aradipatrik.classroomfinder.injection.ACTIVITY_CONTEXT
import javax.inject.Inject
import javax.inject.Named

interface ResourceService {
    fun getStringByName(id: String): String
    fun getStringById(id: Int): String
    fun getTemplateStringById(id: Int, vararg formatArgs: String): String
}

class ResourceServiceImpl @Inject constructor(
    @param:Named(ACTIVITY_CONTEXT) private val context: Context,
    private val resourceIdService: ResourceIdService
) : ResourceService {
    override fun getTemplateStringById(id: Int, vararg formatArgs: String): String =
            context.getString(id, *formatArgs)

    override fun getStringByName(id: String) =
        getStringById(resourceIdService.getResId(id, R.string::class.java))

    override fun getStringById(id: Int): String =
            context.getString(id)
}