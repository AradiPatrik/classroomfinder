package hu.aradipatrik.classroomfinder.service

import biweekly.Biweekly
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject


interface CalendarParserService {
    data class CalendarCourse(
            val courseName: String,
            val lecturerName: String,
            val courseCode: String,
            val classroomCode: String)

    fun parse(calendarAsString: String): List<CalendarCourse>
}

@Suppress("unused")
class CalendarParserServiceImpl @Inject constructor() : CalendarParserService {
    data class CalendarEvent(val summary: String, val location: String, val startDate: Date)

    private val dateFormat = SimpleDateFormat("yyyy-MM-dd")
    private val currentDate = Calendar.getInstance()
    private val springSemesterStart = dateFormat.parse(
            "${currentDate.get(Calendar.YEAR)}-02-3")
    private val springSemesterEnd = dateFormat.parse(
            "${currentDate.get(Calendar.YEAR)}-05-16")
    private val autumnSemesterStart = dateFormat.parse(
            "${currentDate.get(Calendar.YEAR)}-09-02")
    private val autumnSemesterEnd = dateFormat.parse(
            "${currentDate.get(Calendar.YEAR)}-12-07")

    override fun parse(calendarAsString: String): List<CalendarParserService.CalendarCourse> {
        val calendar = Biweekly.parse(calendarAsString).first()
        val currentDate = Date()
        val events = calendar.events
                .filter { it.summary.value.contains("Órarend") }
                .map { CalendarEvent(it.summary.value, it.location.value, it.dateStart.value) }
        val noDuplicates = events.distinctBy { it.summary }
        return noDuplicates
                .filter { if (isInThisAutumnSemester(currentDate)) {
                    isInThisAutumnSemester(it.startDate)
                } else {
                    isInThisSpringSemester(it.startDate)
                }}
                .map {
                    val summaryParts = it.summary.split(" - ")
                    val courseName = summaryParts[0].split(" (")[0]
                    val courseCode = summaryParts[1]
                    val lecturerName = summaryParts[2].split(';')[0].substringBefore(" Dr.")
                    return@map CalendarParserService.CalendarCourse(courseName, lecturerName, courseCode, it.location.substringBefore(","))
                }
    }

    private fun isInThisSpringSemester(date: Date): Boolean {
        return !(date.before(springSemesterStart) || date.after(springSemesterEnd));
    }

    private fun isInThisAutumnSemester(date: Date): Boolean {
        return !(date.before(autumnSemesterStart) || date.after(autumnSemesterEnd));
    }
}