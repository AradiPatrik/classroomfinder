package hu.aradipatrik.classroomfinder.service

import javax.inject.Inject

interface ResourceIdService {
    fun getResId(resName: String, c: Class<*>): Int
}

class ReflectionResourceIdService @Inject constructor() : ResourceIdService {
    override fun getResId(resName: String, c: Class<*>): Int =
            try {
                val idField = c.getDeclaredField(resName)
                idField.getInt(idField)
            } catch (e: Exception) {
                e.printStackTrace()
                -1
            }
}