package hu.aradipatrik.classroomfinder.service

import hu.aradipatrik.classroomfinder.R
import hu.aradipatrik.classroomfinder.db.entities.Classroom
import hu.aradipatrik.classroomfinder.db.pokos.CardStackItemText
import javax.inject.Inject

const val ACTION_PREFIX = "action_"
const val DIRECTION_PREFIX = "direction_"
const val TYPE_PREFIX = "type_"
const val POSITION_PREFIX = "position_"

interface ActionTranslatorService {
    fun translateTurnAction(action: String, type: String): CardStackItemText
    fun translateDestinationReached(classroom: Classroom): CardStackItemText
}

class ActionTranslatorServiceImpl @Inject constructor(
    private val resourceService: ResourceService
) : ActionTranslatorService {
    override fun translateTurnAction(action: String, type: String) = CardStackItemText(
            heading = getCorridorTypeInformationText(type),
            subText = getActionText(action)
    )

    private fun getActionText(action: String) =
            resourceService.getStringByName(ACTION_PREFIX + action)

    private fun getCorridorTypeInformationText(type: String) =
            resourceService.getStringByName(TYPE_PREFIX + type)

    override fun translateDestinationReached(classroom: Classroom) = CardStackItemText(
            heading = getDestinationReachedText(),
            subText = getPositionInformation(classroom)
    )

    private fun getDestinationReachedText() =
            resourceService.getStringById(R.string.destination_reached)

    private fun getPositionInformation(classroom: Classroom) =
            resourceService.getTemplateStringById(
                    R.string.position_information,
                    resourceService.getStringByName(POSITION_PREFIX + classroom.position),
                    resourceService.getStringByName(DIRECTION_PREFIX + classroom.direction))
}
