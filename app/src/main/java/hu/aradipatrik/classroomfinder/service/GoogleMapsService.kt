package hu.aradipatrik.classroomfinder.service

import android.content.Intent
import android.net.Uri
import javax.inject.Inject

private const val MAPS_PACKAGE = "com.google.android.apps.maps"
private const val OPEN_MAPS_TO_BUILDING_NAMED = "geo:46.2530,20.1482?z=10&q=%s"

interface GoogleMapsService {
    fun navigateTo(buildingName: String)
}

class GoogleMapsServiceImpl @Inject constructor(
    private val activityStarterService: ActivityStarterService,
    private val notificationService: NotificationService
) : GoogleMapsService {
    override fun navigateTo(buildingName: String) {
        notificationService.showBackToAppNotification()
        activityStarterService.startActivity(
                createIntentToStartGoogleMapsToBuilding(buildingName)
        )
    }

    private fun createIntentToStartGoogleMapsToBuilding(buildingName: String) =
            Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse(String.format(OPEN_MAPS_TO_BUILDING_NAMED, buildingName))
            ).apply { setPackage(MAPS_PACKAGE) }
}