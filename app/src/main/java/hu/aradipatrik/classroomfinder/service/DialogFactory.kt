package hu.aradipatrik.classroomfinder.service

import android.content.Context
import com.afollestad.materialdialogs.MaterialDialog
import hu.aradipatrik.classroomfinder.injection.ACTIVITY_CONTEXT
import javax.inject.Inject
import javax.inject.Named

interface Dialog {
    fun showDialog(): Unit
}

private class MaterialDialogWrapper(private val materialDialog: MaterialDialog): Dialog {
    override fun showDialog() = materialDialog.show()
}

interface DialogFactory {
    fun createDialog(
            titleResourceId: Int,
            bodyResourceId: Int,
            negativeButtonResourceId: Int,
            positiveButtonResourceId: Int,
            onNegativeClick: () -> Unit = {},
            onPositiveClick: () -> Unit = {}
    ): Dialog
}

class MaterialDialogFactory @Inject constructor(
        @param:Named(ACTIVITY_CONTEXT) private val context: Context) : DialogFactory {
    override fun createDialog(
            titleResourceId: Int,
            bodyResourceId: Int,
            negativeButtonResourceId: Int,
            positiveButtonResourceId: Int,
            onNegativeClick: () -> Unit,
            onPositiveClick: () -> Unit): Dialog =
        MaterialDialogWrapper(MaterialDialog(context)
                .title(titleResourceId)
                .message(bodyResourceId)
                .negativeButton(negativeButtonResourceId)
                .positiveButton(positiveButtonResourceId)
                .negativeButton { onNegativeClick() }
                .positiveButton { onPositiveClick() })

}