package hu.aradipatrik.classroomfinder.service

import android.content.Context
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.DiskBasedCache
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.google.firebase.database.DataSnapshot
import hu.aradipatrik.classroomfinder.injection.ACTIVITY_CONTEXT
import hu.aradipatrik.classroomfinder.util.FValueEventListener
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.lang.RuntimeException
import javax.inject.Inject
import javax.inject.Named
import javax.inject.Singleton
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine

interface HttpService {
    suspend fun getString(url: String): String
}

class VolleyHttpService @Inject constructor(
        @param:Named(ACTIVITY_CONTEXT) private val context: Context
) : HttpService {
    private val que = Volley.newRequestQueue(context)

    override suspend fun getString(url: String): String = withContext(Dispatchers.Default) {
        suspendCoroutine<String> { continuation ->
            val stringRequest = StringRequest(Request.Method.GET, url, Response.Listener<String> {
                continuation.resume(it)
            }, Response.ErrorListener {
                continuation.resumeWithException(RuntimeException("Request error to: $url"))
            })
            que.add(stringRequest)
        }
    }
}