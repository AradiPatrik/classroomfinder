package hu.aradipatrik.classroomfinder.service

import android.content.Context
import android.content.Intent
import androidx.core.content.ContextCompat
import hu.aradipatrik.classroomfinder.injection.ACTIVITY_CONTEXT
import javax.inject.Inject
import javax.inject.Named

interface ActivityStarterService {
    fun startActivity(intent: Intent)
}

class ActivityStarterServiceImpl @Inject constructor(
    @param:Named(ACTIVITY_CONTEXT) private val context: Context
) : ActivityStarterService {
    override fun startActivity(intent: Intent): Unit =
            ContextCompat.startActivity(context, intent, null)
}