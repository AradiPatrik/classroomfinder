package hu.aradipatrik.classroomfinder.service

import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import hu.aradipatrik.classroomfinder.MainActivity
import hu.aradipatrik.classroomfinder.R
import hu.aradipatrik.classroomfinder.db.CHANNEL_ID
import hu.aradipatrik.classroomfinder.injection.ACTIVITY_CONTEXT
import javax.inject.Inject
import javax.inject.Named

interface NotificationService {
    fun showBackToAppNotification()
    fun removeBackToAppNotification()
}

const val BACK_TO_AP_NOTIFICATION_ID = 80085

class NotificationServiceImpl @Inject constructor(
    @param:Named(ACTIVITY_CONTEXT) private val context: Context
) : NotificationService {
    override fun showBackToAppNotification() {
        val intent = Intent(context, MainActivity::class.java)
        val pendingIntent = PendingIntent.getActivity(context, 0, intent, 0)

        val builder = NotificationCompat.Builder(context, CHANNEL_ID)
                .setSmallIcon(R.drawable.baseline_school_white_48)
                .setContentTitle(context.getString(R.string.exit_navigation))
                .setContentText(context.getString(R.string.exit_navigation_help_text))
                .setPriority(NotificationCompat.PRIORITY_MAX)
                .setContentIntent(pendingIntent)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setAutoCancel(true)

        NotificationManagerCompat.from(context)
            .notify(BACK_TO_AP_NOTIFICATION_ID, builder.build())

        builder.build()
    }

    override fun removeBackToAppNotification() =
            NotificationManagerCompat.from(context).cancel(BACK_TO_AP_NOTIFICATION_ID)
}