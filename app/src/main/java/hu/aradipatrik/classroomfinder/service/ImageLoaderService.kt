package hu.aradipatrik.classroomfinder.service

import android.widget.ImageView
import com.squareup.picasso.Picasso
import javax.inject.Inject

interface ImageLoaderService {
    fun loadImageInto(imageUrl: String, imageView: ImageView)
}

class PicassoImageLoaderService @Inject constructor() : ImageLoaderService {
    override fun loadImageInto(imageUrl: String, imageView: ImageView) {
        Picasso.get().load(imageUrl).into(imageView)
    }
}